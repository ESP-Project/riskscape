from django.conf import settings
from django.urls import reverse
from django.http import (HttpResponse, HttpResponseRedirect,
                         HttpResponseServerError)
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import login, logout, get_user_model

from onelogin.saml2.auth import OneLogin_Saml2_Auth
from onelogin.saml2.settings import OneLogin_Saml2_Settings
from onelogin.saml2.utils import OneLogin_Saml2_Utils

def init_saml_auth(req):
    auth = OneLogin_Saml2_Auth(req, custom_base_path=settings.SAML_FOLDER)
    return auth


def prepare_django_request(request):
    # If server is behind proxys or balancers use the HTTP_X_FORWARDED fields
    result = {
        'https': 'on' if request.is_secure() else 'off',
        'http_host': request.META['HTTP_HOST'],
        'script_name': request.META['PATH_INFO'],
        'server_port': request.META['SERVER_PORT'],
        'get_data': request.GET.copy(),
        # Uncomment if using ADFS as IdP, https://github.com/onelogin/python-saml/pull/144
        # 'lowercase_urlencoding': True,
        'post_data': request.POST.copy()
    }
    return result

@csrf_exempt
def samlsso(request):
    req = prepare_django_request(request)
    auth = init_saml_auth(req)
    errors = []
    error_reason = None
    not_auth_warn = False
    success_slo = False
    attributes = False
    paint_logout = False

    # SSO from link on page
    if 'sso' in req['get_data']:
        #return HttpResponseRedirect(auth.login())
        return HttpResponseRedirect(auth.get_sso_url())
    # SSO from identity provider
    elif 'acs' in req['get_data']:
        request_id = None
        if 'AuthNRequestID' in request.session:
            request_id = request.session['AuthNRequestID']


        auth.process_response(request_id=request_id)
        errors = auth.get_errors()
        not_auth_warn = not auth.is_authenticated()
        error_reason = auth.get_last_error_reason()

        if not errors:
            if 'AuthNRequestID' in request.session:
                del request.session['AuthNRequestID']
            request.session['samlUserdata'] = auth.get_attributes()
            request.session['samlNameId'] = auth.get_nameid()
            request.session['samlNameIdFormat'] = auth.get_nameid_format()
            request.session['samlNameIdNameQualifier'] = auth.get_nameid_nq()
            request.session['samlNameIdSPNameQualifier'] = auth.get_nameid_spnq()
            request.session['samlSessionIndex'] = auth.get_session_index()
            if 'RelayState' in req['post_data'] and req['post_data']['RelayState'] and OneLogin_Saml2_Utils.get_self_url(req) != req['post_data']['RelayState']:
                return HttpResponseRedirect(auth.redirect_to(req['post_data']['RelayState']))
        else:
            if auth.get_settings().is_debug_active():
                error_reason = auth.get_last_error_reason()

    saml_nameid = None
    if 'samlNameId' in request.session and request.session['samlNameId']:
        paint_logout = True
        saml_nameid = request.session['samlNameId']

    # Get RiskScape User with username from SAML nameID 
    User = get_user_model()
    saml_user = None

    if saml_nameid is not None:
        try:
            saml_user = User.objects.get(username=saml_nameid)
        except User.DoesNotExist:
            saml_user = User.objects.create_user(saml_nameid)
            saml_user.is_active = True
            saml_user.save()

    # login to RiskScape
    if saml_user is not None and saml_user.is_active:
        saml_user.backend = 'django.contrib.auth.backends.ModelBackend'
        request.session.flush()
        login(request, saml_user)
        return redirect(reverse('home'))
    else:
        errors.append('User  not found')
        error_reason = 'Invalid SAML NameID'
        not_auth_warn = True
    
    # error - no auth
    return render(request, 'ssoerror.html', {'errors': errors, 'error_reason': error_reason, 'not_auth_warn': not_auth_warn})


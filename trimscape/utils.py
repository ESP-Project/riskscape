def nest(cards, nested=None):
    if nested is None:
        nested = []

    if cards == []:
        return nested

    nested.append([cards[:6][:3], cards[:6][3:]])
    return nest(cards[6:], nested)


def pad_dashboard(l=None):
    if l is None:
        return [None] * 6
    if len(l) < 6:
        l += [None] * (6 - len(l))
    elif len(l) == 6 or len(l) % 6 == 0:
        return l
    else:
        l += [None] * (6 - (len(l) % 6))
    return l

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    pass
'''
    dependencies = [
        ('registration', '0003_registration_profile_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='MyRegistrationSupplement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('full_name', models.CharField(max_length=250, verbose_name='Full Name')),
                ('institution', models.CharField(max_length=250, verbose_name='Institutional Affiliation')),
                ('registration_profile', models.OneToOneField(editable=False, verbose_name='registration profile', to='registration.RegistrationProfile', related_name='_riskscape_registration_myregistrationsupplement_supplement')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
'''

{% load i18n %}
{% blocktrans %}
Your RiskScape account is now approved. You can log in using the following link
{% endblocktrans %}
http://{{site.domain}}{% url 'auth_login' %}


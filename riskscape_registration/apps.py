# myapp/apps.py
from django.apps import AppConfig


class RiskscapeRegistrationAppConfig(AppConfig):

    name = 'riskscape_registration'
    verbose_name = 'Riskscape Registration'


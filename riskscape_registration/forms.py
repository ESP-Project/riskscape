from django.forms import ModelForm
from registration.forms import RegistrationFormUniqueEmail
from riskscape_registration.models import Profile

class RiskscapeRegistrationForm(RegistrationFormUniqueEmail):
    pass

class UserProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ['full_name','institution']

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import coc.models


class Migration(migrations.Migration):

    dependencies = [
        ('coc', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='uploadcoc',
            options={'ordering': ['site_name']},
        ),
        migrations.AlterField(
            model_name='uploadcoc',
            name='data_file',
            field=models.FileField(upload_to='coc', storage=coc.models.OverwriteStorage(), max_length=255),
        ),
    ]

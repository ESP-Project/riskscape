# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coc', '0004_uploadcoc_disease'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uploadcoc',
            name='disease',
            field=models.CharField(choices=[('hepc', 'Hepatitis C')], max_length=15),
        ),
    ]

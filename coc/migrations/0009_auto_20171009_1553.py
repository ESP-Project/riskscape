# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def update_existing_disease_files(apps, schema_editor):
    DiseaseFile = apps.get_model('coc', 'UploadCOC')
    COCDisease = apps.get_model('coc', 'COCDisease')
    DiseaseFile.objects.update(coc_disease=COCDisease.objects.get(slug='hepc'))


class Migration(migrations.Migration):
    dependencies = [
        ('coc', '0008_uploadcoc_coc_disease'),
    ]

    operations = [
        migrations.RunPython(update_existing_disease_files, None),
    ]

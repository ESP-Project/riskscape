# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def add_coc_diseases(apps, schema_editor):
    COCDisease = apps.get_model("coc", "COCDisease")
    diseases = (("hepc", "Hepatitis C"), ('hiv', "HIV"), ('diab', "Diabetes"))

    for slug, name in diseases:
        COCDisease.objects.create(name=name, slug=slug)


class Migration(migrations.Migration):
    dependencies = [
        ('coc', '0006_cocdisease'),
    ]

    operations = [
        migrations.RunPython(add_coc_diseases, None),
    ]

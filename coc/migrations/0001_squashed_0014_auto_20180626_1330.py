# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-12-05 16:12
from __future__ import unicode_literals

import coc.models
from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


# Functions from the following migrations need manual copying.
# Move them and any dependencies into this file, then update the
# RunPython operations to refer to the local versions:
# coc.migrations.0007_auto_20171009_1546
# coc.migrations.0009_auto_20171009_1553

def add_coc_diseases(apps, schema_editor):
    COCDisease = apps.get_model("coc", "COCDisease")
    diseases = (("hepc", "Hepatitis C"), ('hiv', "HIV"), ('diab', "Diabetes"))

    for slug, name in diseases:
        COCDisease.objects.create(name=name, slug=slug)


def update_existing_disease_files(apps, schema_editor):
    DiseaseFile = apps.get_model('coc', 'UploadCOC')
    COCDisease = apps.get_model('coc', 'COCDisease')
    DiseaseFile.objects.update(coc_disease=COCDisease.objects.get(slug='hepc'))

class Migration(migrations.Migration):

    replaces = [('coc', '0001_initial'), ('coc', '0002_auto_20170328_0654'), ('coc', '0003_auto_20170424_0918'), ('coc', '0004_uploadcoc_disease'), ('coc', '0005_auto_20170519_1135'), ('coc', '0006_cocdisease'), ('coc', '0007_auto_20171009_1546'), ('coc', '0008_uploadcoc_coc_disease'), ('coc', '0009_auto_20171009_1553'), ('coc', '0010_auto_20171009_1559'), ('coc', '0011_auto_20171009_1616'), ('coc', '0012_auto_20171113_1537'), ('coc', '0013_auto_20171113_1557'), ('coc', '0014_auto_20180626_1330')]

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='UploadCOC',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_file', models.FileField(max_length=255, storage=coc.models.OverwriteStorage(), upload_to='coc')),
                ('site_name', models.CharField(choices=settings.SITENAMES, max_length=3)),
                ('disease', models.CharField(choices=[('hepc', 'Hepatitis C')], max_length=15)),
            ],
            options={
                'ordering': ['site_name'],
            },
        ),
        migrations.CreateModel(
            name='COCDisease',
            fields=[
                #('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('slug', models.CharField(max_length=10, primary_key=True, serialize=False)),
            ],
        ),
        migrations.RunPython(
            code=add_coc_diseases,
        ),
        migrations.AddField(
            model_name='uploadcoc',
            name='coc_disease',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE,related_name='files', to='coc.COCDisease'),
        ),
        migrations.RunPython(
            code=update_existing_disease_files,
        ),
        migrations.RemoveField(
            model_name='uploadcoc',
            name='disease',
        ),
        #migrations.AlterField(
        #    model_name='uploadcoc',
        #    name='coc_disease',
        #    field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='coc.COCDisease'),
        #),
        #migrations.RemoveField(
        #    model_name='cocdisease',
        #    name='id',
        #),
        #migrations.AlterField(
        #    model_name='cocdisease',
        #    name='slug',
        #    field=models.CharField(max_length=5, primary_key=True, serialize=False),
        #),
        migrations.AlterField(
            model_name='uploadcoc',
            name='data_file',
            field=models.FileField(max_length=255, storage=coc.models.OverwriteStorage(), upload_to=coc.models.path_and_rename),
        ),
        #migrations.AlterField(
        #    model_name='uploadcoc',
        #    name='coc_disease',
        #    field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='files', to='coc.COCDisease'),
        #),
        migrations.AlterField(
            model_name='uploadcoc',
            name='data_file',
            field=models.FileField(max_length=255, upload_to=coc.models.path_and_rename),
        ),
        #migrations.AlterField(
        #    model_name='cocdisease',
        #    name='slug',
        #    field=models.CharField(max_length=10, primary_key=True, serialize=False),
        #),
    ]

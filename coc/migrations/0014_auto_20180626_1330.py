# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coc', '0013_auto_20171113_1557'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cocdisease',
            name='slug',
            field=models.CharField(max_length=10, primary_key=True, serialize=False),
        ),
    ]

import os
from datetime import date

from django.db import models, transaction
from django.conf import settings

SITE_CHOICES = settings.SITENAMES

class SAEDataManager(models.Manager):

    @transaction.atomic
    def load_data(self):
        pass
        
class SAEData(models.Model):
    condition = models.CharField('Condition',
                                 max_length=50,
                                 choices = settings.SAE_CONDITIONS,
                                 null=False,
                                 blank=False)
    year = models.CharField('Year',
                            max_length=4,
                            null=False,
                            blank=False)
    npats = models.PositiveIntegerField('Number of Patients',
                                  null=True,
                                )
    prevalence = models.FloatField('Prevalence',
                                  null=True,
                                )
    ci_low = models.FloatField('CI Low',
                                  null=True,
                                )
    ci_high = models.FloatField('CI High',
                                  null=True,
                                )
    demographic1 = models.CharField('Demographic1',
                              max_length=50,
                              choices = settings.SAE_DEMOGRAPHICS,
                              null=False,
                              blank=False)
    demographic1_value = models.CharField('Demographic1 Value',
                              max_length=200,
                              null=False,
                              blank=False)
    demographic2 = models.CharField('Demographic2',
                              max_length=50,
                              choices = settings.SAE_DEMOGRAPHICS,
                              null=False,
                              blank=False)
    demographic2_value = models.CharField('Demographic2 Value',
                              max_length=200,
                              null=False,
                              blank=False)
    class Meta:
        abstract = True

class StateLevel(SAEData):

    objects = SAEDataManager()

    class Meta:
        unique_together = ['condition','year', 
                           'demographic1', 'demographic1_value',
                           'demographic2', 'demographic2_value']

class ZipLevel(SAEData):
    zip = models.CharField('Zip code',
                              max_length=5,
                              null=False,
                              blank=False)

    objects = SAEDataManager()

    class Meta:
        unique_together = ['zip','condition','year',
                           'demographic1', 'demographic1_value',
                           'demographic2', 'demographic2_value']

class MuniLevel(SAEData):
    muni = models.CharField('Municipality',
                              max_length=30,
                              null=False,
                              blank=False)

    objects = SAEDataManager()

    class Meta:
        unique_together = ['muni','condition','year',
                           'demographic1', 'demographic1_value',
                           'demographic2', 'demographic2_value']

def path_and_rename(instance, filename):
    upload_to = 'upload'
    filename = '{}_{}'.format(date.today().strftime('%Y-%m-%d'), filename)
    return os.path.join(upload_to, filename)


#class UploadDataManager(models.Manager):

class UploadData(models.Model):
    data_file = models.FileField(max_length=255, upload_to=path_and_rename)
    processed = models.DateTimeField(null=True)
    status = models.IntegerField(choices=((0, "Pending"), (1, "Running"), (2, "Success"), (3, "Failure")), default=0)

    #objects = UploadDataManager()

    def __str__(self):
        return "{}".format(self.get_status_display())



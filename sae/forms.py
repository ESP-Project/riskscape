from django import forms
from sae.models import UploadData


class UploadRawDataForm(forms.ModelForm):
    class Meta:
        model = UploadData
        fields = ['data_file']

import zipfile
from datetime import datetime, date
import os
from shutil import copyfile

from django.db import DataError
from django.db import IntegrityError
from django.db import transaction
from django.db import connection
from django.db.models import Count
from django_rq import job, enqueue
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.contrib.auth import get_user_model

from sae.models import UploadData, StateLevel, ZipLevel, MuniLevel
from trimscape.settings import DATA_SITE_COUNT, MEDIA_ROOT
from trimscape.settings import NEW_DATA_EMAIL_LIST, DEFAULT_FROM_EMAIL, ADMIN_EMAIL_LIST
from trimscape.settings import ORG_SYSTEM, MAIN_SITE_URL, SAE_DASHBOARD_CONDITIONS
from analysis.models import TimeseriesSAECache, MapSAECache, CurrentRateSAECache
from analysis.models import SAESummaryOutcome
from analysis.forms import SAEFilterQueryForm, SAEOutcomeQueryForm, SAEGeoForm
from analysis.view_utils import get_time_range
from analysis import jobs

import logging 
logger = logging.getLogger('riskscape')

class LoadError(Exception):
    """Exception raised for errors in the input.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

def _user_email():
    User = get_user_model()
    users = User.objects.filter(is_active=True)
    email_list = []
    for u in users:
        if u.username !='esp' and u.email:
            email_list.append(u.email)
    return email_list

def _send_email(error_message=None):
    # construct email
    if error_message:
        subject = "SAE Data Upload Error on {}".format(ORG_SYSTEM)
        recipient_list = ADMIN_EMAIL_LIST
        from_email = DEFAULT_FROM_EMAIL
        context ={'site_name':ORG_SYSTEM, 
                  'site_url':MAIN_SITE_URL,
                  'error_message': error_message}
        text_msg = render_to_string('load_error_email.txt', context)
        html_msg = render_to_string('load_error_email.html', context)
    else:
        subject = "New SAE Data Upload on {}".format(ORG_SYSTEM)
        if len(NEW_DATA_EMAIL_LIST) > 0:
            recipient_list = NEW_DATA_EMAIL_LIST
        else:
            recipient_list = _user_email()
        from_email = DEFAULT_FROM_EMAIL
        context ={'site_name':ORG_SYSTEM, 'site_url':MAIN_SITE_URL}
        text_msg = render_to_string('new_data_email.txt', context)
        html_msg = render_to_string('new_data_email.html', context)
    # send email
    for to_email in recipient_list:
        send_mail(
            subject,
            text_msg,
            from_email,
            [to_email],
            fail_silently=False,
            html_message=html_msg,
        )

def process_data():
    loadable_data = UploadData.objects.filter(status=0)
    _load_data.delay([ld.pk for ld in loadable_data])


@job('historical')
def _load_data(uploads):
    uploaded = len(uploads)
    for upload in uploads:
        ud = UploadData.objects.get(pk=upload)

        try:
            ud.status = 1
            ud.save()

            _etl(ud)
            ud.data_file.delete(save=True)
            ud.status = 2
            ud.save()
            _send_email()
        except (LoadError, ValueError, DataError, IntegrityError) as e:
            ud.status = 3
            uploaded -= 1
            _send_email(error_message=str(e))
            raise LoadError(e)
        finally:
            ud.processed = datetime.now()
            ud.save()

    if uploaded > 0:
        _update_outcomes()
        _update_map_cache()

def _etl(ud):
    if zipfile.is_zipfile(ud.data_file.path):
        backup_file = os.path.join(MEDIA_ROOT + 'upload', '_backup.zip')
        copyfile(ud.data_file.path, backup_file)
        path_parts = ud.data_file.path.split('/')
        path = "{}/{}".format('/'.join(path_parts[:-1]), path_parts[-1:][0].split('.')[0])
        with zipfile.ZipFile(ud.data_file.path) as nrs_zip:
            nrs_zip.extractall(path)
            nrs_names = nrs_zip.namelist()
            # truncate result cache tables
            MapSAECache.objects.all().delete()
            CurrentRateSAECache.objects.all().delete()
            TimeseriesSAECache.objects.all().delete()
            # load files
            for fname in nrs_names:
                if fname == 'state_level.csv':
                    #StateLevel.objects.all().delete()
                    fname = path + '/' + fname
                    sql = """COPY sae_statelevel(condition,year,demographic1,demographic1_value,demographic2, demographic2_value, npats, prevalence, ci_low, ci_high) FROM '{}' DELIMITER ',' CSV HEADER""".format(fname)
                    with connection.cursor() as cursor:
                        cursor.execute(sql)
                elif fname == 'zip_level.csv' or fname == 'ZCTA_level.csv':
                    #ZipLevel.objects.all().delete()
                    fname = path + '/' + fname
                    sql = """COPY sae_ziplevel(zip, condition,year,demographic1,demographic1_value,demographic2, demographic2_value, npats, prevalence, ci_low, ci_high) FROM '{}' DELIMITER ',' CSV HEADER""".format(fname)
                    with connection.cursor() as cursor:
                        cursor.execute(sql)
                elif fname == 'muni_level.csv':
                    #MuniLevel.objects.all().delete()
                    fname = path + '/' + fname
                    sql = """COPY sae_munilevel(muni, condition,year,demographic1,demographic1_value,demographic2, demographic2_value, npats, prevalence, ci_low, ci_high) FROM '{}' DELIMITER ',' CSV HEADER""".format(fname)
                    with connection.cursor() as cursor:
                        cursor.execute(sql)

def _update_outcomes():
    SAESummaryOutcome.objects.exclude(field__in=[condition for condition in SAE_DASHBOARD_CONDITIONS]).delete()
    for condition, form_values in SAE_DASHBOARD_CONDITIONS.items():
        _update_outcome.delay(condition, form_values)


def _update_map_cache(condition_name=None):
    MapSAECache.objects.all().delete()
    for condition, form_values in SAE_DASHBOARD_CONDITIONS.items():
        if condition_name is not None and not condition == condition_name:
            continue
        outcome_form = SAEOutcomeQueryForm(form_values['outcome'])
        filter_form = SAEFilterQueryForm(form_values['filter'])
        geo_form = SAEGeoForm({"geo_area":"zip"})
        geo_area = 'zip'
        if outcome_form.is_valid() and filter_form.is_valid() and geo_form.is_valid():
            month_range, year_range, start, end = get_time_range('zip')
            year = end.year
            mrc = MapSAECache.objects.get_cached_results(outcome_form=outcome_form, 
                                                    filter_form=filter_form,
                                                    geo_form=geo_form,
                                                    year=year)
            if mrc is None or mrc.result is None:
                mrc = MapSAECache.objects.cache_results(outcome_form=outcome_form, 
                                                    filter_form=filter_form, 
                                                    geo_form= geo_form,
                                                    result=None,
                                                    year=year)
                jobs.generate_sae_map_cache.delay(mrc.pk, geo_area, year)


@job
def _update_outcome(condition, form_values):
    today = date.today()
    outcome_form = SAEOutcomeQueryForm(form_values['outcome'])
    filter_form = SAEFilterQueryForm(form_values['filter'])
    if outcome_form.is_valid() and filter_form.is_valid():
        pass
    else:
        print("{} error".format(condition))

    try:
        so = SAESummaryOutcome.current.get(field=condition,
                                        date__year=today.year)
    except SAESummaryOutcome.DoesNotExist:
        so = SAESummaryOutcome(field=condition)
    so.save()


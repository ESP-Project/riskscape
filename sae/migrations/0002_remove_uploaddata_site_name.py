# Generated by Django 4.2 on 2023-08-17 20:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sae', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='uploaddata',
            name='site_name',
        ),
    ]

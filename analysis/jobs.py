from math import ceil, floor

from django_rq import job
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

from analysis.forms import FilterQueryForm, OutcomeQueryForm
from analysis.forms import SAEFilterQueryForm, SAEOutcomeQueryForm
from analysis.models import MapResultCache, CurrentRateResultCache, TimeseriesResultCache
from analysis.models import MapSAECache, CurrentRateSAECache, TimeseriesSAECache
from analysis.view_utils import census_satisfied, set_decimals, filter_by_location
from analysis.view_utils import sufficient_data, get_data_time_range, get_time_range
from patient_data.models import RiskscapeData, TrimtrackerData
from sae.models import StateLevel, ZipLevel, MuniLevel
import logging 

logger = logging.getLogger('riskscape')

@job
def generate_sae_map_cache(cache_pk, geo_area, year=None):
    mrc = MapSAECache.objects.get(pk=cache_pk)
    outcome_form = SAEOutcomeQueryForm(mrc.outcome)
    filter_form = SAEFilterQueryForm(mrc.filter)
    condition = None
    if outcome_form.is_valid():
        condition = outcome_form.cleaned_data['condition']
    else:
        logger.debug("Condition is not available for SAE map cache result.")

    map_data = []
    md_range = {"min": float(100), "max": float(0)}
    statewide = {"numerator": 0, "denominator": 0}
    q_range = {}

    if filter_form.is_valid():
        demographic1 = filter_form.cleaned_data['demographic1']
        demographic2 = filter_form.cleaned_data['demographic2']
    else:
        demographic1 = 'overall'
        demographic2 = 'overall'

    if condition:
        if demographic1 and demographic1 == 'overall':
            demographic1_value = 'overall'
        elif demographic1 and filter_form.is_valid():
            demographic1_value = filter_form.cleaned_data[demographic1+'1']
        else:
            demographic1_value = 'overall'
        if demographic2 and demographic2 == 'overall':
            demographic2_value = 'overall'
        elif demographic2 and filter_form.is_valid():
            demographic2_value = filter_form.cleaned_data[demographic2+'2']
        else:
            demographic2_value = 'overall'

    if demographic1_value and demographic2_value:
        swap1 = (demographic1 == "gender" and demographic2 == "age_group")
        swap2 = (demographic1 == "race" and demographic2 == "age_group")
        swap3 = (demographic1 == "race" and demographic2 == "gender")
        if swap1 or swap2 or swap3:
            d1 = demographic1
            demographic1 = demographic2
            demographic2 = d1
            v1 = demographic1_value
            demographic1_value = demographic2_value
            demographic2_value = v1

        try:
            state_data = StateLevel.objects.get(
                    condition=condition,
                    year= year,
                    demographic1=demographic1,
                    demographic1_value__iexact=demographic1_value,
                    demographic2=demographic2,
                    demographic2_value__iexact=demographic2_value,
                )
            state_prevalence = state_data.prevalence*100
            state_npats = state_data.npats
            state_ci_lower = state_data.ci_low*100
            state_ci_upper = state_data.ci_high*100
            statewide['prevalence'] = set_decimals(state_prevalence)
            statewide['npats'] = state_npats
            statewide['ci_lower'] = set_decimals(state_ci_lower)
            statewide['ci_upper'] = set_decimals(state_ci_upper)
        except ObjectDoesNotExist:
            logger.debug("State level data is not available for map cache")
        
        geo_data = None
        if geo_area == 'zip':
            geo_data = ZipLevel.objects.filter(
                    condition=condition,
                    year= year,
                    demographic1=demographic1,
                    demographic1_value__iexact=demographic1_value,
                    demographic2=demographic2,
                    demographic2_value__iexact=demographic2_value,
                    prevalence__isnull=False,
                    prevalence__gt=0,
                ).order_by("prevalence")
        elif geo_area == 'muni':
            geo_data = MuniLevel.objects.filter(
                    condition=condition,
                    year= year,
                    demographic1=demographic1,
                    demographic1_value__iexact=demographic1_value,
                    demographic2=demographic2,
                    demographic2_value__iexact=demographic2_value,
                    prevalence__isnull=False,
                    prevalence__gt=0,
                ).order_by("prevalence")
        if geo_data:
            step_size = len(geo_data)/5
            step_count = 1
            quintile = 1
            map_dada = []
            for zd in geo_data:
                if step_count > step_size:
                    quintile +=1
                    step_count = 1
                else:
                    step_count +=1
                npats = int(zd.npats) if zd.npats else None
                npats = f"{npats:,d}" if npats else None
                prevalence = round(zd.prevalence*100,1) if zd.prevalence else None
                ci_lower = round(zd.ci_low*100,1) if zd.ci_low else None
                ci_upper = round(zd.ci_high*100,1) if zd.ci_high else None
                if geo_area == 'zip':
                    zip_muni = zd.zip
                else:
                    zip_muni = zd.muni.replace(' ', '-').upper() if zd.muni else None
                map_data.append({
                        'zip': zip_muni,
                        'prevalence': prevalence,
                        'population': npats,
                        'lowerci': ci_lower,
                        'upperci': ci_upper,
                        'sae': True,
                        'quintile':quintile,
                    })
                if zd.prevalence and zd.prevalence > 0:
                    md_range['min'] = min(float(zd.prevalence*100), md_range['min'])
                    md_range['max'] = max(float(zd.prevalence*100), md_range['max'])
            if md_range['max'] == md_range['min'] == float(100):
                md_range['min'] = 0
            elif (md_range['max'] == md_range['min']) and md_range['min'] > 0:
                md_range['min'] = 0
            if md_range['max'] > md_range['min']:
                for md in map_data:
                    if md['prevalence'] is not None:
                        md['normalized'] = (md['prevalence'] - md_range['min']) / (md_range['max'] - md_range['min'])
                        md['prevalence'] = set_decimals(md['prevalence'])
            else:
                md_range['min'] = md_range['max']
            md_range['max'] = int(ceil(md_range['max']))
            md_range['min'] = int(floor(md_range['min']))

            # quintile min/max
            q1_min = 100
            q1_max = 0
            q2_min = 100
            q2_max = 0
            q3_min = 100
            q3_max = 0
            q4_min = 100
            q4_max = 0
            q5_min = 100
            q5_max = 0
            for md in map_data:
                if md["quintile"] == 1:
                    q1_min = min(float(md["prevalence"]), q1_min) if md["prevalence"] else q1_min
                    q1_max = max(float(md["prevalence"]), q1_max) if md["prevalence"] else q1_max
                elif md["quintile"] == 2:
                    q2_min = min(float(md["prevalence"]), q2_min) if md["prevalence"] else q2_min
                    q2_max = max(float(md["prevalence"]), q2_max) if md["prevalence"] else q2_max
                elif md["quintile"] == 3:
                    q3_min = min(float(md["prevalence"]), q3_min) if md["prevalence"] else q3_min
                    q3_max = max(float(md["prevalence"]), q3_max) if md["prevalence"] else q3_max
                elif md["quintile"] == 4:
                    q4_min = min(float(md["prevalence"]), q4_min) if md["prevalence"] else q4_min
                    q4_max = max(float(md["prevalence"]), q4_max) if md["prevalence"] else q4_max
                elif md["quintile"] == 5:
                    q5_min = min(float(md["prevalence"]), q5_min) if md["prevalence"] else q5_min
                    q5_max = max(float(md["prevalence"]), q5_max) if md["prevalence"] else q5_max

            q_range['q1'] = {"min":q1_min,"max":q1_max}
            q_range['q2'] = {"min":q2_min,"max":q2_max}
            q_range['q3'] = {"min":q3_min,"max":q3_max}
            q_range['q4'] = {"min":q4_min,"max":q4_max}
            q_range['q5'] = {"min":q5_min,"max":q5_max}

        else:
            logger.debug("Zip level data is not available for map cache")


    result = {"map_data": map_data,
              "map_range": md_range,
              "q_range": q_range,
              "statewide": statewide,
              "sae": True}

    mrc.result = result
    mrc.save()

@job
def generate_map_cache(cache_pk, geo_area, month=None, year=None):
    mrc = MapResultCache.objects.get(pk=cache_pk)
    outcome_form = OutcomeQueryForm(mrc.outcome, is_auth=True)
    filter_form = FilterQueryForm(mrc.filter, is_auth=True)
    if outcome_form.is_valid() and filter_form.is_valid():
        pass
    map_data = []
    md_range = {"min": float(100), "max": float(0)}
    statewide = {"numerator": 0, "denominator": 0}

    geo_place_type = settings.GEO_PLACES[geo_area]['GEO_AREA'] if settings.GEO_PLACES else None
    geo_place_id = settings.GEO_PLACES[geo_area]['GEO_PLACE_REFID'] if settings.GEO_PLACES else None
    if month and year:
        data_model = TrimtrackerData
        if geo_place_id:
            filtered_model = data_model.objects.filter(place=geo_place_id, year=year, month=month)
        else:
            filtered_model = data_model.objects.filter(year=year, month=month)
    else:
        data_model = RiskscapeData
        if geo_place_id:
            filtered_model = data_model.objects.filter(place=geo_place_id)
        else:
            filtered_model = data_model.objects.all()
    
    if (geo_area == 'tract') or (geo_place_type == 'tract'):
        geo_codes = data_model.objects.qs_ratio(
            qs=filter_form.process(
                filtered_model.filter(census_tract__population__isnull=False)
            ).values('census_tract', 'census_tract__coverage', 'census_tract__catchment', 'census_tract__population'),
            outcome_filters=outcome_form)
    else:
        geo_codes = data_model.objects.qs_ratio(
            qs=filter_form.process(
                filtered_model.filter(census__population__isnull=False)
            ).values('census', 'census__coverage', 'census__catchment', 'census__population'),
            outcome_filters=outcome_form)

    for zip_code in geo_codes:
        statewide["numerator"], statewide["denominator"] = statewide["numerator"] + zip_code["numerator"], \
                                                           statewide["denominator"] + zip_code["denominator"]
        if not census_satisfied(zip_code):
            if zip_code['denominator'] > 99 and not settings.MENDS:
                if zip_code['numerator'] > 0:
                    prevalence = (100.0 / float(zip_code['denominator']))
                    numerator = None
                    denominator = None
                else:
                    prevalence = 0
                    numerator = 0
                    denominator = zip_code['denominator']
            else:
                prevalence = None
                numerator = None
                denominator = None
        else:
            prevalence = (float(zip_code['numerator']) * 100.0 / float(zip_code['denominator']))
            numerator = zip_code['numerator']
            denominator = zip_code['denominator']
            md_range['min'], md_range['max'] = min(prevalence, md_range['min']), max(prevalence, md_range['max'])

        if (geo_area == 'tract') or (geo_place_type =='tract'):
            if prevalence is not None:
                map_data.append(
                {'zip': zip_code['census_tract'], 'coverage': zip_code['census_tract__coverage'],
                'population': zip_code['census_tract__population'], 'catchment': zip_code['census_tract__catchment'],
                'prevalence': prevalence, 'numerator': numerator, 'denominator': denominator})
        else:
            if prevalence is not None:
                map_data.append(
                {'zip': zip_code['census'], 'coverage': zip_code['census__coverage'],
                'population': zip_code['census__population'], 'catchment': zip_code['census__catchment'],
                'prevalence': prevalence, 'numerator': numerator, 'denominator': denominator})

    if md_range['max'] == md_range['min'] == float(100):
        md_range['min'] = 0

    if md_range['max'] > md_range['min']:
        for md in map_data:
            if md['prevalence'] is not None:
                md['normalized'] = (md['prevalence'] - md_range['min']) / (md_range['max'] - md_range['min'])
                md['prevalence'] = set_decimals(md['prevalence'])
    else:
        md_range['min'] = md_range['max']
    md_range['max'] = int(ceil(md_range['max']))
    md_range['min'] = int(floor(md_range['min']))
    # sort map data by prevalence
    sorted_map_data = sorted(map_data, key=lambda d: float(d['prevalence']))
    # calculate quintiles
    step_size = len(sorted_map_data)/5
    step_count = 1
    quintile = 1
    q1_min = 100
    q1_max = 0
    q2_min = 100
    q2_max = 0
    q3_min = 100
    q3_max = 0
    q4_min = 100
    q4_max = 0
    q5_min = 100
    q5_max = 0
    q_range = {}
    for md in sorted_map_data:
        if step_count > step_size:
            # increment quintile
            quintile +=1
            #reset step count
            step_count = 1
        else:
            # increment step count
            step_count +=1
        md['quintile'] = quintile
        if md["quintile"] == 1:
            q1_min = min(float(md["prevalence"]), q1_min) if md["prevalence"] else q1_min
            q1_max = max(float(md["prevalence"]), q1_max) if md["prevalence"] else q1_max
        elif md["quintile"] == 2:
            q2_min = min(float(md["prevalence"]), q2_min) if md["prevalence"] else q2_min
            q2_max = max(float(md["prevalence"]), q2_max) if md["prevalence"] else q2_max
        elif md["quintile"] == 3:
            q3_min = min(float(md["prevalence"]), q3_min) if md["prevalence"] else q3_min
            q3_max = max(float(md["prevalence"]), q3_max) if md["prevalence"] else q3_max
        elif md["quintile"] == 4:
            q4_min = min(float(md["prevalence"]), q4_min) if md["prevalence"] else q4_min
            q4_max = max(float(md["prevalence"]), q4_max) if md["prevalence"] else q4_max
        elif md["quintile"] == 5:
            q5_min = min(float(md["prevalence"]), q5_min) if md["prevalence"] else q5_min
            q5_max = max(float(md["prevalence"]), q5_max) if md["prevalence"] else q5_max
    q_range['q1'] = {"min":q1_min,"max":q1_max}
    q_range['q2'] = {"min":q2_min,"max":q2_max}
    q_range['q3'] = {"min":q3_min,"max":q3_max}
    q_range['q4'] = {"min":q4_min,"max":q4_max}
    q_range['q5'] = {"min":q5_min,"max":q5_max}


    try:
        statewide['ratio'] = set_decimals(float(statewide['numerator']) * 100.0 / float(statewide['denominator']))
    except ZeroDivisionError:
        statewide['ratio'] = 0

    result = {"map_data": sorted_map_data,
              "map_range": md_range,
              "q_range": q_range,
              "statewide": statewide}

    mrc.result = result
    mrc.save()

def save_strat_result(crc, result, strat_id):
    if strat_id == 1:
        crc_stratify = crc.stratify
    elif strat_id == 2:
        crc_stratify = crc.stratify2
    elif strat_id == 3:
        crc_stratify = crc.stratify
        crc_stratify2 = crc.stratify2
    else:
        return False

    for strat in result:
        if (crc_stratify in strat):
            strat_name = None
            for v in RiskscapeData._meta.get_field(crc_stratify).choices:
                if v[0] == strat[crc_stratify]:
                    strat[crc_stratify] = v[1]
                    break
            if strat[crc_stratify] is None:
                strat[crc_stratify] = 'Unknown'
        else:
            strat[crc_stratify] = 'Unknown'

        if (strat_id == 3) and (crc_stratify2 in strat):
            strat_name = None
            for v in RiskscapeData._meta.get_field(crc_stratify2).choices:
                if v[0] == strat[crc_stratify2]:
                    strat[crc_stratify2] = v[1]
                    break
            if strat[crc_stratify2] is None:
                strat[crc_stratify2] = 'Unknown'
        elif (strat_id == 3) and not (crc_stratify2 in strat):
            strat[crc_stratify2] = 'Unknown'
    if sufficient_data(result):
        strat_name = crc_stratify
        if strat_id == 3:
            strat_name = crc_stratify + " & " + crc_stratify2
        all_strats = {strat_name: 'All', 'numerator': 0, 'denominator': 0}
        for item in result:
            all_strats['numerator'] = all_strats['numerator'] + item['numerator']
            all_strats['denominator'] = all_strats['denominator'] + item['denominator']
            if not census_satisfied(item):
                item['numerator'] = -1
                item['denominator'] = -1

        if strat_id == 1:
            try:
                crc.result = [dict(x) for x in result if x['denominator'] > -1]
                if not crc.stratify == 'none':
                    crc.result.append(all_strats)
                else:
                    all_strats['none'] = "Unstratified"
                    crc.result = all_strats
            except TypeError:
                crc.result = False

        elif strat_id == 2:
            try:
                crc.result2 = [dict(x) for x in result if x['denominator'] > -1]
                if not crc.stratify2 == 'none':
                    crc.result2.append(all_strats)
                else:
                    all_strats['none'] = "Unstratified"
                    crc.result2 = all_strats
            except TypeError:
                crc.result2 = False

        elif strat_id == 3:
            try:
                crc.result3 = [dict(x) for x in result if x['denominator'] > -1]
                if not crc.stratify2 == 'none':
                    crc.result3.append(all_strats)
                else:
                    all_strats['none'] = "Unstratified"
                    crc.result3 = all_strats
            except TypeError:
                crc.result3 = False
        else:
            return False


def save_sae_strat_result(crc, result, result_id=None):
    strat_result = []
    demographic_result_overall = None

    for strat in result:
        d1 = True #strat.demographic1 == crc.stratify or strat.demographic1 == 'overall'
        d2 = True #strat.demographic2 == crc.stratify2 or strat.demographic2 == 'overall'
        if d1 and d2:
            demographic = None
            demographic2 = None
            demographic2_value = None
            if strat.demographic1 == 'overall' and strat.demographic2 == 'overall':
                if crc.stratify != 'overall' and crc.stratify2 != 'overall':
                    pass
                elif crc.stratify != 'overall':
                    demographic = crc.stratify
                    demographic_value = 'overall'
                elif crc.stratify2 != 'overall':
                    demographic = crc.stratify2
                    demographic_value = 'overall'
                else:
                    demographic = 'overall'
                    demographic_value = 'overall'
            elif strat.demographic1 != 'overall' and strat.demographic2 != 'overall':
                if result_id == 3:
                    demographic = strat.demographic1
                    demographic_value = strat.demographic1_value.lower()
                    demographic2 = strat.demographic2
                    demographic2_value = strat.demographic2_value.lower()
                if result_id == 2:
                    demographic = strat.demographic1
                    demographic_value = strat.demographic1_value.lower()
            elif strat.demographic1 != 'overall':
                demographic = strat.demographic1
                demographic_value = strat.demographic1_value.lower()
            elif strat.demographic2 != 'overall':
                demographic = strat.demographic2
                demographic_value = strat.demographic2_value.lower()

            if demographic and demographic_value != 'overall':
                if demographic_value == 'caucasian': demographic_value = 'white'
                if demographic2_value == 'caucasian': demographic2_value = 'white'
                if demographic_value == 'f': demographic_value = 'female'
                if demographic_value == 'm': demographic_value = 'male'
                if demographic2_value == 'f': demographic2_value = 'female'
                if demographic2_value == 'm': demographic2_value = 'male'
                demographic_result = {
                    demographic:demographic_value,
                    demographic2:demographic2_value,
                    "prevalence":strat.prevalence*100 if strat.prevalence else None,
                    "ci_low":round(strat.ci_low*100,1) if strat.ci_low else None,
                    "ci_high":round(strat.ci_high*100,1) if strat.ci_high else None,
                    "npats":strat.npats}
                strat_result.append(demographic_result)
            elif demographic and demographic_value == 'overall':
                demographic_result_overall = {
                    demographic:demographic_value,
                    demographic2:demographic2_value,
                    "prevalence":strat.prevalence*100 if strat.prevalence else None,
                    "ci_low":round(strat.ci_low*100,1) if strat.ci_low else None,
                    "ci_high":round(strat.ci_high*100,1) if strat.ci_high else None,
                    "npats":strat.npats}
    if demographic_result_overall: # append overall last
        strat_result.append(demographic_result_overall)
    # save result
    if result_id and result_id==3:
        if (len(strat_result) > 0):
            crc.result3 = strat_result
        else:
            crc.result3 = False
    elif result_id and result_id==2:
        if (len(strat_result) > 0):
            crc.result2 = strat_result
        else:
            crc.result2 = False
    else:
        if (len(strat_result) > 0):
            crc.result = strat_result
        else:
            crc.result = False

    crc.save()

def save_sae_data(crc, level, year, condition, location, stratify1, stratify2):
    # query set for SAE model
    if level == 'state':
        sae_qs = StateLevel.objects.filter(year=year,
                                           condition=condition)
    elif level == 'zip':
        sae_qs = ZipLevel.objects.filter(year=year,
                                         condition=condition,
                                         zip=location)
    elif level == 'muni':
        sae_qs = MuniLevel.objects.filter(year=year,
                                         condition=condition,
                                         muni=location)
    else:
        return None

    if stratify1 != 'overall' and stratify2 != 'overall':
        # 2 stratifiers
        # save result for stratify1
        result1 = sae_qs.filter(year=year,
                                  demographic1=stratify1,
                                  demographic2='overall',
                                  )
        result1 = result1 | sae_qs.filter(year=year,
                                         demographic1='overall',
                                         demographic2='overall',
                                        )
        save_sae_strat_result(crc, result1, 1)
        # save result for stratify2
        result2 = sae_qs.filter(year=year,
                                condition=condition,
                                demographic1=stratify2,
                                demographic2='overall',
                                )
        result2 = result2 | sae_qs.filter(year=year,
                                          condition=condition,
                                          demographic1='overall',
                                          demographic2='overall',
                                          )
        save_sae_strat_result(crc, result2, 2)
        # save result for stratify1 and stratify2
        result3 = sae_qs.filter(year=year,
                                condition=condition,
                                demographic1=stratify1,
                                demographic2=stratify2,
                                )
        result3 = result3 | sae_qs.filter(year=year,
                                          condition=condition,
                                          demographic1='overall',
                                          demographic2='overall',
                                        )
        save_sae_strat_result(crc, result3, 3)
    else:
        # 1 stratifier
        # save result for stratify1 or stratify2
        result = sae_qs.filter(year=year,
                               condition=condition,
                               demographic1=stratify1,
                               demographic2=stratify2,
                               )
        if stratify1 != 'overall' or stratify2 != 'overall':
            result = result | sae_qs.filter(year=year,
                                            condition=condition,
                                            demographic1='overall',
                                            demographic2='overall',
                                            )
        save_sae_strat_result(crc, result, 1)

@job
def generate_current_sae_cache(cache_pk, geo_area, year):

    crc = CurrentRateSAECache.objects.get(pk=cache_pk)
    outcome_form = SAEOutcomeQueryForm(crc.outcome)
    filter_form = SAEFilterQueryForm(crc.filter)
    condition = None
    location = crc.location
    stratify1 = crc.stratify 
    stratify2 = crc.stratify2
    if crc.stratify == 'gender' and crc.stratify2 == 'age_group':
        stratify1 = 'age_group'
        stratify2 = 'gender'
    elif crc.stratify == 'race' and crc.stratify2 == 'age_group':
        stratify1 = 'age_group'
        stratify2 = 'race'
    elif crc.stratify == 'race' and crc.stratify2 == 'gender':
        stratify1 = 'gender'
        stratify2 = 'race'
    
    if outcome_form.is_valid():
        condition = outcome_form.cleaned_data['condition']
    else:
        logger.debug("Condition is not availble for current sae cache result.")

    if stratify1:
        year = str(year)
        try:
            if location == 'Massachusetts':
                save_sae_data(crc, 'state', year, condition, location, stratify1, stratify2)
            elif geo_area == 'zip':
                save_sae_data(crc, 'zip', year, condition, location, stratify1, stratify2)
            elif geo_area == 'muni':
                save_sae_data(crc, 'muni', year, condition, location, stratify1, stratify2)
        except ObjectDoesNotExist:
            logger.debug("Data is not available for current sae cache")

@job
def generate_current_rate_cache(cache_pk, geo_area, month, year):

    crc = CurrentRateResultCache.objects.get(pk=cache_pk)
    outcome_form = OutcomeQueryForm(crc.outcome, is_auth=True)
    filter_form = FilterQueryForm(crc.filter, is_auth=True)
    if outcome_form.is_valid() and filter_form.is_valid():
        pass
    if month and year:
        data_model = TrimtrackerData
    else:
        data_model = RiskscapeData

    filtered = filter_by_location(crc.location, data_model, filter_form, geo_area, year, month)

    # stratification 1
    strat1 = 'age_group' if crc.stratify == 'none' else crc.stratify
    result1 = data_model.objects.qs_ratio(filtered.values(strat1), outcome_form).distinct().order_by(strat1)
    save_strat_result(crc, result1, 1)

    # stratification 2
    strat2 = crc.stratify2
    if strat2 != 'none':
        result2 = data_model.objects.qs_ratio(filtered.values(strat2), outcome_form).distinct().order_by(strat2)
        save_strat_result(crc, result2, 2)
        result3 = data_model.objects.qs_ratio(filtered.values(strat1,strat2), outcome_form).distinct().order_by(strat1,strat2)
        save_strat_result(crc, result3, 3)

    crc.save()

@job
def generate_timeseries_sae_cache(cache_pk, geo_area):
    trc = TimeseriesSAECache.objects.get(pk=cache_pk)
    outcome_form = SAEOutcomeQueryForm(trc.outcome)
    filter_form = SAEFilterQueryForm(trc.filter)
    condition = None
    stratify1 = None 
    stratify2 = None
    location = trc.location
    if outcome_form.is_valid():
        condition = outcome_form.cleaned_data['condition']
    else:
        logger.debug("Condition is not availble for timeseries sae cache result.")
        return None
    if filter_form.is_valid() and condition:
        stratify1 = filter_form.cleaned_data['demographic1']
        stratify2 = 'overall' #filter_form.cleaned_data['demographic2']
    else:
        logger.debug("Filter form is not valid for timeseries cache result.")
        return None

    # query set for SAE model
    sae_qs = None
    result = {}
    level = None
    if location == 'Massachusetts':
        level = 'state'
        sae_qs = StateLevel.objects.filter(condition=condition,
                                           demographic1=stratify1,
                                           demographic2=stratify2
                                           ).order_by('year')
    elif geo_area == 'zip':
        level = 'zip'
        sae_qs = ZipLevel.objects.filter(condition=condition,
                                         demographic1=stratify1,
                                         demographic2=stratify2,
                                         zip=location).order_by('year')
    elif geo_area == 'muni':
        level = 'muni'
        sae_qs = MuniLevel.objects.filter(condition=condition,
                                          demographic1=stratify1,
                                          demographic2=stratify2,
                                          muni=location).order_by('year')

    if sae_qs:
        month_range, year_range, start, end = get_time_range(level)
        strat_values = {}
        if stratify1 == 'gender':
            strat_values = dict((x,y) for x,y in settings.SAE_GENDER)
        elif stratify1 == 'age_group':
            strat_values = dict((x,y) for x,y in settings.SAE_AGE_GROUP)
        elif stratify1 == 'race':
            strat_values = dict((x,y) for x,y in settings.SAE_RACE)
        elif stratify1 == 'overall':
            strat_values = {'overall':'overall'}

        irange = iter(year_range)
        for strat,v in strat_values.items():
            #result[strat.lower()] = []
            strat_val = strat.lower()
            if strat_val == 'caucasian': strat_val = 'white'
            if strat_val == 'f': strat_val = 'female'
            if strat_val == 'm': strat_val = 'male'
            result[strat_val] = []
        while True:
            try:
                calendar = next(irange)
                for strat,v in strat_values.items():
                    #result[strat.lower()].append(_get_sae_filler_point(calendar))
                    strat_val = strat.lower()
                    if strat_val == 'caucasian': strat_val = 'white'
                    if strat_val == 'f': strat_val = 'female'
                    if strat_val == 'm': strat_val = 'male'
                    result[strat_val].append(_get_sae_filler_point(calendar))
            except StopIteration:
                break
        for sd in sae_qs:
            strat = sd.demographic1_value
            if strat:
                strat = strat.lower()
                if strat == 'caucasian': strat = 'white'
                if strat == 'f': strat = 'female'
                if strat == 'm': strat = 'male'
                for filler in result[strat]:
                    year = int(sd.year)
                    if filler['year'] == year and sd.prevalence:
                        filler['prevalence'] = sd.prevalence*100
                        filler['ci_low'] = round(sd.ci_low*100,1) if sd.ci_low else None
                        filler['ci_high'] = round(sd.ci_high*100,1) if sd.ci_high else None
                        break
    
        try:
            trc.result = [dict(x) for x in result]
        except ValueError:
            trc.result = result
        trc.save()

@job
def generate_timeseries_cache(cache_pk, geo_area):
    trc = TimeseriesResultCache.objects.get(pk=cache_pk)
    outcome_form = OutcomeQueryForm(trc.outcome, is_auth=True)
    filter_form = FilterQueryForm(trc.filter, is_auth=True)
    if outcome_form.is_valid() and filter_form.is_valid():
        pass

    filtered = filter_by_location(trc.location, TrimtrackerData, filter_form, geo_area)
    has_census = False
    geo_place_id = settings.GEO_PLACES[geo_area]['GEO_PLACE_REFID'] if settings.GEO_PLACES else None
    month_range, quarter_range, year_range, *extra = get_data_time_range(geo_place_id)
    if not trc.stratify == 'none':
        if trc.time_chunk == 'month':
            filtered = filtered.values(trc.stratify, 'year', 'month').order_by('year', 'month', trc.stratify)
            range = month_range
        elif trc.time_chunk == "year":
            filtered = filtered.values(trc.stratify, 'year').order_by('year', trc.stratify)
            range = year_range
        elif trc.time_chunk == "quarter":
            filtered = filtered.values(trc.stratify, 'year', 'quarter').order_by('year', 'quarter', trc.stratify)
            range = quarter_range

        result = {}
        strat_choices = filter_form.model._meta.get_field(trc.stratify).choices
        if filter_form.cleaned_data[trc.stratify]:
            strat_choices = [choice for choice in strat_choices if str(choice[0]) in filter_form.cleaned_data[trc.stratify]]
        for strat in [x[1] for x in strat_choices]:
            irange = iter(range)
            result[strat] = []
            while True:
                try:
                    calendar = next(irange)
                    result[strat].append(_get_filler_point(calendar, trc.time_chunk))
                except StopIteration:
                    break

        for point in TrimtrackerData.objects.qs_ratio(filtered, outcome_form):
            if point[trc.stratify] is None:
                del point[trc.stratify]
                continue
            strat = [x[1] for x in filter_form.model._meta.get_field(trc.stratify).choices
                     if x[0] == point[trc.stratify]][0]
            del point[trc.stratify]

            if not census_satisfied(point):
                point['numerator'] = -1
                point['denominator'] = -1
            else:
                has_census = True
            for filler in result[strat]:
                if filler['year'] == point['year'] and filler[trc.time_chunk] == point[trc.time_chunk]:
                    filler['numerator'], filler['denominator'] = point['numerator'], point['denominator']
                    break
    else:
        if trc.time_chunk == "month":
            filtered = filtered.values('year', 'month').order_by('year', 'month')
            range = month_range
        if trc.time_chunk == "year":
            filtered = filtered.values('year').order_by('year')
            range = year_range
        elif trc.time_chunk == "quarter":
            filtered = filtered.values('year', 'quarter').order_by('year', 'quarter')
            range = quarter_range
        result = {'All': []}

        irange = iter(range)
        result = {'All': []}
        while True:
            try:
                calendar = next(irange)
                result['All'].append(_get_filler_point(calendar, trc.time_chunk))
            except StopIteration:
                break
        for point in TrimtrackerData.objects.qs_ratio(filtered, outcome_form):
            if not census_satisfied(point):
                point['numerator'] = -1
                point['denominator'] = -1
            else:
                has_census = True
            for filler in result['All']:
                if filler['year'] == point['year'] and filler[trc.time_chunk] == point[trc.time_chunk]:
                    filler['numerator'], filler['denominator'] = point['numerator'], point['denominator']
                    break

    if not has_census:
        result = []
    try:
        trc.result = [dict(x) for x in result]
    except ValueError:
        trc.result = result

    trc.save()


def _match_calendar_timeseries(calendar, point, chunk):
    if calendar.year != point['year']:
        return False
    if chunk == 'month':
        if calendar.month != point['month']:
            return False
    elif chunk == 'quarter':
        if calendar.quarter != point['quarter']:
            return False
    return True


def _get_filler_point(calendar, chunk):
    return {"year": calendar.year, chunk: getattr(calendar, chunk), 'numerator': -1, 'denominator': -1}

def _get_sae_filler_point(calendar):
    return {"year": calendar.year, 'month': calendar.month, 'prevalence': -1}


import json
from datetime import date
import hashlib
import arrow
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import F, Sum
from django.conf import settings
from django.db.models import Max

from analysis.forms import OutcomeQueryForm, FilterQueryForm, GeoForm
from analysis.forms import SAEOutcomeQueryForm, SAEFilterQueryForm, SAEGeoForm
from patient_data.models import RiskscapeData
from sae.models import StateLevel

import logging 

logger = logging.getLogger('riskscape')

DASHBOARD_CONDITIONS = {}
for cond in settings.RISKSCAPE_CONDITIONS:
    DASHBOARD_CONDITIONS[cond] = settings.RISKSCAPE_DASHBOARD_CONDITIONS[cond]

class CurrentSummaryOutcomeManager(models.Manager):
    def get_queryset(self):
        try:
            latest_date = SummaryOutcome.objects.latest('date').date
        except SummaryOutcome.DoesNotExist:
            latest_date = date.today()
        return super(CurrentSummaryOutcomeManager, self).get_queryset() \
            .filter(date=latest_date).order_by('field')

class SummaryOutcome(models.Model):
    date = models.DateField(auto_now=True)
    field = models.CharField(max_length=50)
    chart_type = models.CharField(max_length=1, choices=(('D', 'Donut'), ('S', 'Sparkline')))

    objects = models.Manager()
    current = CurrentSummaryOutcomeManager()

    class Meta:
        ordering = ['-date']
        unique_together = ('field', 'date')

    class ThresholdError(Exception):
        pass

    def __str__(self):
        return '{} for {}-{}'.format(self.display_name, self.date.year, self.date.month)

    def save(self, *args, **kwargs):
        self.chart_type = DASHBOARD_CONDITIONS[self.field].get("chart", "D")
        super(SummaryOutcome, self).save(*args, **kwargs)

    @property
    def _get_forms(self):
        if self.field not in DASHBOARD_CONDITIONS:
            return None
        form_values = DASHBOARD_CONDITIONS[self.field]
        outcome_form = OutcomeQueryForm(form_values['outcome'])
        filter_form = FilterQueryForm(form_values['filter'])
        geo_form = GeoForm({"geo_area":settings.GEO_AREA_DEFAULT})
        if outcome_form.is_valid() and filter_form.is_valid() and geo_form.is_valid():
            return outcome_form, filter_form, geo_form
        return None

    def get_geo_location(self, geo_form):
        geo_area = geo_form.cleaned_data['geo_area'] if geo_form.is_valid() else settings.GEO_AREA_DEFAULT
        if geo_area == 'zip' or geo_area == 'tract':
            geo_location = settings.GEO_LOCATION
        else:
            geo_location = settings.GEO_PLACES[geo_area]['GEO_LOCATION']
        return geo_location

    def fetch_value(self, offset):
        outcome_form, filter_form, geo_form = self._get_forms
        geo_location = self.get_geo_location(geo_form)
        trc = TimeseriesResultCache.objects.get_cached_results(outcome_form=outcome_form, 
                                                               filter_form=filter_form,
                                                               geo_form=geo_form,
                                                               location=geo_location,
                                                               time_chunk="month",
                                                               stratify="none")
        if trc is None or not trc.result:
            return None

        result = trc.result
        offset = -offset - 1

        try:
            result = result['All'][offset:][0]
            if -1 in (result['numerator'], result['denominator']):
                raise self.ThresholdError
            return (result['numerator'] / result['denominator']) * 100
        except (AttributeError, KeyError, ZeroDivisionError):
            return None
        except self.ThresholdError:
            return -1

    @property
    def trendline(self):
        if self.chart_type == "D":
            return None

        outcome_form, filter_form, geo_form = self._get_forms
        geo_location = self.get_geo_location(geo_form)
        trc = TimeseriesResultCache.objects.get_cached_results(outcome_form=outcome_form, 
                                                               filter_form=filter_form,
                                                               geo_form=geo_form,
                                                               location=geo_location,
                                                               time_chunk="month",
                                                               stratify="none")
        if trc is None or not trc.result:
            return ''

        result = trc.result
        ratio = []
        trend_lookback = arrow.get(self.date.year, self.date.month, 15).shift(months=-37)
        for t in result['All']:
            if self._is_current(t, trend_lookback):
                if t['denominator'] == -1:
                    ratio.append(-1)
                else:
                    ratio.append(t['numerator'] / t['denominator'])

        return ratio

    def _is_current(self, t, lookback):
        if self.chart_type == "D":
            return False

        if arrow.get(t['year'], t['month'], 15) >= lookback:
            return True

        return False

    @property
    def display_name(self):
        if self.field in DASHBOARD_CONDITIONS:
            return DASHBOARD_CONDITIONS[self.field].get("name", self.field)
        return None

    @property
    def values_of_interest(self):
        if self.field not in DASHBOARD_CONDITIONS:
            return None
        ff = FilterQueryForm(DASHBOARD_CONDITIONS[self.field]['filter'])
        if ff.is_valid():
            pass
        filter_strings = []
        for key, value in ff.pretty_print_query().items():
            filter_strings.append("{}: {}".format(key, value))

        return "<br/>".join(filter_strings)

    def _display_value(self, value):
        if value is None:
            return ''
        elif value < 0:
            return value
        return format(value, '.2f')

    @property
    def this_month(self):
        return self._display_value(self.fetch_value(0))

    @property
    def most_recent(self):
        return "{}%".format(self._display_value(self.fetch_value(6)))

    @property
    def next_most_recent(self):
        return "{}%".format(self._display_value(self.fetch_value(12)))

    @property
    def least_recent(self):
        return "{}%".format(self._display_value(self.fetch_value(24)))

class SAECurrentSummaryOutcomeManager(models.Manager):
    def get_queryset(self):
        try:
            latest_date = SAESummaryOutcome.objects.latest('date').date
        except SAESummaryOutcome.DoesNotExist:
            latest_date = date.today()
        return super(SAECurrentSummaryOutcomeManager, self).get_queryset() \
            .filter(date=latest_date).order_by('field')

class SAESummaryOutcome(models.Model):
    date = models.DateField(auto_now=True)
    field = models.CharField(max_length=50)
    chart_type = models.CharField(max_length=1, choices=(('D', 'Donut'), ('S', 'Sparkline')))

    objects = models.Manager()
    current = SAECurrentSummaryOutcomeManager()

    class Meta:
        ordering = ['-date']
        unique_together = ('field', 'date')

    class ThresholdError(Exception):
        pass

    def __str__(self):
        return '{} for {}'.format(self.display_name, self.date.year)

    def save(self, *args, **kwargs):
        self.chart_type = settings.SAE_DASHBOARD_CONDITIONS[self.field].get("chart", "D")
        super(SAESummaryOutcome, self).save(*args, **kwargs)

    @property
    def _get_forms(self):
        if self.field not in settings.SAE_DASHBOARD_CONDITIONS:
            return None
        form_values = settings.SAE_DASHBOARD_CONDITIONS[self.field]
        outcome_form = SAEOutcomeQueryForm(form_values['outcome'])
        filter_form = SAEFilterQueryForm(form_values['filter'])
        geo_form = SAEGeoForm({"geo_area":"zip"})
        if outcome_form.is_valid() and filter_form.is_valid() and geo_form.is_valid():
            return outcome_form, filter_form, geo_form
        return None

    def get_geo_location(self, geo_form):
        geo_area = geo_form.cleaned_data['geo_area'] if geo_form.is_valid() else settings.GEO_AREA_DEFAULT
        if geo_area == 'zip' or geo_area == 'tract':
            geo_location = settings.GEO_LOCATION
        else:
            geo_location = settings.GEO_PLACES[geo_area]['GEO_LOCATION']
        return geo_location

    def fetch_value(self):
        outcome_form, filter_form, geo_form = self._get_forms
        last_year = StateLevel.objects.aggregate(Max('year'))['year__max']
        mrc = MapSAECache.objects.get_cached_results(outcome_form=outcome_form, 
                                                    filter_form=filter_form,
                                                    geo_form=geo_form,
                                                    year=last_year)
        if mrc is None or not mrc.result:
            return None
        try:
            prevalence = mrc.result['statewide']['prevalence']
            return float(prevalence)
        except (AttributeError, KeyError):
            return None

    @property
    def trendline(self):
        if self.chart_type == "D":
            return None

        outcome_form, filter_form, geo_form = self._get_forms
        geo_location = self.get_geo_location(geo_form)
        trc = TimeseriesSAECache.objects.get_cached_results(outcome_form=outcome_form, 
                                                               filter_form=filter_form,
                                                               geo_form=geo_form,
                                                               location=geo_location,
                                                               stratify="none")
        if trc is None or not trc.result:
            return ''

        result = trc.result
        prevalence = []
        trend_lookback = arrow.get(self.date.year).shift(years=-3)
        for t in result:
            if self._is_current(t, trend_lookback):
                    prevalence.append(t['prevalence'])

        return prevalence

    def _is_current(self, t, lookback):
        if self.chart_type == "D":
            return False

        if arrow.get(t['year']) >= lookback:
            return True

        return False

    @property
    def display_name(self):
        if self.field in settings.SAE_DASHBOARD_CONDITIONS:
            return settings.SAE_DASHBOARD_CONDITIONS[self.field].get("name", self.field)
        return None

    @property
    def values_of_interest(self):
        if self.field not in settings.SAE_DASHBOARD_CONDITIONS:
            return None
        sae_filter = settings.SAE_DASHBOARD_CONDITIONS[self.field]['filter'] 
        ff = SAEFilterQueryForm(sae_filter)
        if ff.is_valid():
            pass
        filter_strings = []
        #for key, value in sae_filter.items():
        #    if value != 'overall':
        #        filter_strings.append("{}: {}".format(key, value))
        d1 = sae_filter['demographic1']
        d2 = sae_filter['demographic2']
        if d1  == 'overall' and d2 == 'overall':
            filter_strings.append("Age &ge;18")
        else:
            if d1 != 'overall':
                k1 = d1 + '1'
                if k1 in sae_filter:
                    filter_strings.append(d1 + ': ' + sae_filter[k1])
            if d2 != 'overall':
                k2 = d2 + '2'
                if k2 in sae_filter:
                    filter_strings.append(d2 + ': ' + sae_filter[k2])

        return "<br/>".join(filter_strings)

    def _display_value(self, value):
        if value is None:
            return ''
        elif value < 0:
            return value
        return format(value, '.2f')

    @property
    def this_month(self):
        return self._display_value(self.fetch_value())


class CensusManager(models.Manager):
    def get_caught_pop(self):
        return self.aggregate(caught=Sum('catchment'))['caught']


class Census(models.Model):
    zip = models.CharField(max_length=25, primary_key=True)
    population = models.PositiveIntegerField(blank=True, null=True)
    catchment = models.PositiveIntegerField(blank=True, null=True)
    coverage = models.CharField(max_length=7, default="Unknown")

    objects = CensusManager()

    class Meta:
        ordering = ['zip']

    def __str__(self):
        return '{}'.format(self.zip)

    def save(self, *args, **kwargs):
        if self.population and self.catchment and int(self.population) > 0:
            self.coverage = format(int(self.catchment) * 100 / int(self.population), '.1f')
        super(Census, self).save(*args, **kwargs)

class CensusTract(models.Model):
    tract = models.CharField(max_length=25, primary_key=True)
    population = models.PositiveIntegerField(blank=True, null=True)
    catchment = models.PositiveIntegerField(blank=True, null=True)
    coverage = models.CharField(max_length=7, default="Unknown")

    objects = CensusManager()

    class Meta:
        ordering = ['tract']

    def __str__(self):
        return '{}'.format(self.tract)

    def save(self, *args, **kwargs):
        if self.population and self.catchment and int(self.population) > 0:
            self.coverage = format(int(self.catchment) * 100 / int(self.population), '.1f')
        super(CensusTract, self).save(*args, **kwargs)

class Muni(models.Model):
    muni = models.CharField(max_length=25, primary_key=True)
    population = models.PositiveIntegerField(blank=True, null=True)
    catchment = models.PositiveIntegerField(blank=True, null=True)
    coverage = models.CharField(max_length=7, default="Unknown")

    objects = CensusManager()

    class Meta:
        ordering = ['muni']

    def __str__(self):
        return '{}'.format(self.tract)

    def save(self, *args, **kwargs):
        if self.population and self.catchment and int(self.population) > 0:
            self.coverage = format(int(self.catchment) * 100 / int(self.population), '.1f')
        super(Muni, self).save(*args, **kwargs)


class City(models.Model):
    city = models.CharField(max_length=32, primary_key=True)
    zips = models.ManyToManyField(Census)
    tracts = models.ManyToManyField(CensusTract)

    class Meta:
        ordering = ['city']

    def __str__(self):
        return self.city


class ResultCacheManager(models.Manager):
    def cache_results(self, **kwargs):
        try:
            rc = self.get(**kwargs)
        except ObjectDoesNotExist:
            rc = self.create(**kwargs)
        if kwargs['result'] is not None:
            rc.result = kwargs['result']
            rc.last_accessed = date.today()
            rc.save()

        return rc

    def _get_cache(self, **kwargs):
        rc = self.get(**kwargs)
        if rc.result is not None:
            rc.viewed = F('viewed') + 1
            rc.last_accessed = date.today()
            rc.save()

        return rc

    def get_cached_results(self, **kwargs):
        try:
            trc = self._get_cache(**kwargs)
            return trc
        except self.model.DoesNotExist:
            return None
        except self.model.MultipleObjectsReturned:
            return None

    def get(self, **kwargs):
        try:
            kwargs['outcome_hash'] = kwargs['outcome_form'].query_hash()
            kwargs['filter_hash'] = kwargs['filter_form'].query_hash()
            kwargs['geo_hash'] = hashlib.md5(json.dumps(kwargs['geo_form'].cleaned_data).encode('utf-8')).hexdigest()
            del kwargs['outcome_form']
            del kwargs['filter_form']
            del kwargs['geo_form']
        except KeyError:
            pass

        return super().get(**kwargs)


class ResultCache(models.Model):
    outcome_form = models.JSONField()
    filter_form = models.JSONField()
    geo_form = models.JSONField()
    outcome_hash = models.CharField(max_length=32)
    filter_hash = models.CharField(max_length=32)
    geo_hash = models.CharField(max_length=32)
    result = models.JSONField(null=True, blank=True)
    result2 = models.JSONField(null=True, blank=True)
    result3 = models.JSONField(null=True, blank=True)
    viewed = models.IntegerField(default=1)
    last_accessed = models.DateField(auto_created=True, null=True, blank=True)

    objects = ResultCacheManager()

    class Meta:
        abstract = True

    @property
    def outcome(self):
        return json.loads(self.outcome_form)

    @property
    def filter(self):
        return json.loads(self.filter_form)

    def save(self, *args, **kwargs):
        if self.pk is None:
            self.outcome_hash = self.outcome_form.query_hash()
            self.filter_hash = self.filter_form.query_hash()
            self.geo_hash = hashlib.md5(json.dumps(self.geo_form.cleaned_data).encode('utf-8')).hexdigest()
            self.outcome_form = json.dumps(self.outcome_form.cleaned_data, sort_keys=True)
            self.filter_form = json.dumps(self.filter_form.cleaned_data, sort_keys=True)
            self.geo_form = json.dumps(self.geo_form.cleaned_data, sort_keys=True)
        super(ResultCache, self).save(*args, **kwargs)


class CurrentRateResultCache(ResultCache):
    location = models.CharField(max_length=32)
    stratify = models.CharField(max_length=32, null=True, blank=True)
    stratify2 = models.CharField(max_length=32, null=True, blank=True)
    month = models.IntegerField(null=True)
    year = models.IntegerField(null=True)

    class Meta:
        unique_together = ('outcome_hash', 'filter_hash', 'location', 
                           'stratify', 'stratify2', 'geo_hash', 'month', 'year')


class TimeseriesResultCache(ResultCache):
    location = models.CharField(max_length=32)
    time_chunk = models.CharField(max_length=10)
    stratify = models.CharField(max_length=16, null=True, blank=True)

    class Meta:
        unique_together = ('outcome_hash', 'filter_hash', 'location', 'stratify', 'time_chunk', 'geo_hash')


class MapResultCache(ResultCache):
    month = models.IntegerField(null=True)
    year = models.IntegerField(null=True)

    class Meta:
        unique_together = ('outcome_hash', 'filter_hash','geo_hash', 'month', 'year')


class SAECacheManager(models.Manager):
    def cache_results(self, **kwargs):
        try:
            rc = self.get(**kwargs)
        except ObjectDoesNotExist:
            rc = self.create(**kwargs)
        if kwargs['result'] is not None:
            rc.result = kwargs['result']
            rc.last_accessed = date.today()
            rc.save()

        return rc

    def _get_cache(self, **kwargs):
        rc = self.get(**kwargs)
        if rc.result is not None:
            rc.viewed = F('viewed') + 1
            rc.last_accessed = date.today()
            rc.save()

        return rc

    def get_cached_results(self, **kwargs):
        try:
            trc = self._get_cache(**kwargs)
            return trc
        except self.model.DoesNotExist:
            return None
        except self.model.MultipleObjectsReturned:
            return None

    def get(self, **kwargs):
        try:
            kwargs['geo_hash'] = hashlib.md5(json.dumps(kwargs['geo_form'].cleaned_data).encode('utf-8')).hexdigest()
            kwargs['outcome_hash'] = hashlib.md5(json.dumps(kwargs['outcome_form'].cleaned_data).encode('utf-8')).hexdigest()
            kwargs['filter_hash'] = hashlib.md5(json.dumps(kwargs['filter_form'].cleaned_data).encode('utf-8')).hexdigest()
            del kwargs['outcome_form']
            del kwargs['filter_form']
            del kwargs['geo_form']
        except KeyError:
            logger.debug('result cache get KeyError')
            pass

        return super().get(**kwargs)

class SAECache(models.Model):
    outcome_form = models.JSONField()
    filter_form = models.JSONField()
    geo_form = models.JSONField()
    outcome_hash = models.CharField(max_length=32)
    filter_hash = models.CharField(max_length=32)
    geo_hash = models.CharField(max_length=32)
    result = models.JSONField(null=True, blank=True)
    result2 = models.JSONField(null=True, blank=True)
    result3 = models.JSONField(null=True, blank=True)
    viewed = models.IntegerField(default=1)
    last_accessed = models.DateField(auto_created=True, null=True, blank=True)

    objects = SAECacheManager()

    class Meta:
        abstract = True

    @property
    def outcome(self):
        return json.loads(self.outcome_form)

    @property
    def filter(self):
        return json.loads(self.filter_form)

    def save(self, *args, **kwargs):
        if self.pk is None:
            self.outcome_hash = hashlib.md5(json.dumps(self.outcome_form.cleaned_data).encode('utf-8')).hexdigest()
            self.filter_hash = hashlib.md5(json.dumps(self.filter_form.cleaned_data).encode('utf-8')).hexdigest()
            self.geo_hash = hashlib.md5(json.dumps(self.geo_form.cleaned_data).encode('utf-8')).hexdigest()
            self.outcome_form = json.dumps(self.outcome_form.cleaned_data, sort_keys=True)
            self.filter_form = json.dumps(self.filter_form.cleaned_data, sort_keys=True)
            self.geo_form = json.dumps(self.geo_form.cleaned_data, sort_keys=True)
        super(SAECache, self).save(*args, **kwargs)

class MapSAECache(SAECache):
    year = models.IntegerField(null=True)

    class Meta:
        unique_together = ('outcome_hash', 'filter_hash','geo_hash', 'year')

class CurrentRateSAECache(SAECache):
    location = models.CharField(max_length=32)
    stratify = models.CharField(max_length=32, null=True, blank=True)
    stratify2 = models.CharField(max_length=32, null=True, blank=True)
    year = models.IntegerField(null=True)

    class Meta:
        unique_together = ('outcome_hash', 'filter_hash', 'location', 
                           'stratify', 'stratify2', 'geo_hash', 'year')


class TimeseriesSAECache(SAECache):
    location = models.CharField(max_length=32)
    stratify = models.CharField(max_length=32, null=True, blank=True)
    stratify2 = models.CharField(max_length=32, null=True, blank=True)

    class Meta:
        unique_together = ('outcome_hash', 'filter_hash', 'location', 
                            'stratify', 'stratify2', 'geo_hash')

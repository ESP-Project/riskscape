from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.http import JsonResponse, HttpResponseBadRequest
from django.shortcuts import render, redirect, get_object_or_404, redirect, HttpResponse
from django.template.response import TemplateResponse, SimpleTemplateResponse
from django.conf import settings

from analysis import jobs
from analysis.models import MapResultCache, CurrentRateResultCache, TimeseriesResultCache
from analysis.models import MapSAECache, CurrentRateSAECache, TimeseriesSAECache
from analysis.models import SAESummaryOutcome, SummaryOutcome, DASHBOARD_CONDITIONS
from analysis.view_utils import current_rates, timeseries, _graph_view, unpack_params, conditional_decorator
from analysis.view_utils import get_time_range, sae_current_rates, _sae_graph_view, sae_timeseries
from sae.models import StateLevel, ZipLevel, MuniLevel
from trimscape import utils

from . import forms
from django_otp.decorators import otp_required
from riskscape_registration.models import Profile

import sys, csv
from datetime import datetime
import logging 

logger = logging.getLogger('riskscape')

@login_required
def sae_home(request, open_modal=None):
    this_open_modal = True if open_modal else False
    if not settings.TWO_FACTOR_ENABLED or request.user.is_verified():
        outcome_form, filter_form, geo_form = forms.process_sae_queryform(request)

        # outcomes for dashboard
        values = SAESummaryOutcome.current.all()
        outcomes = {}
        try:
            for v in values:
                try:
                    outcome = {'name': v.field,
                           'display_name': v.display_name,
                           'interest': v.values_of_interest,
                           'value': v.this_month,
                           'trendline': v.trendline,
                           'chart_type': v.chart_type
                           }
                    outcomes[v.field] = outcome
                except TypeError:
                    pass

            donuts = [outcomes.get("Diabetes", None),
                  outcomes.get("Smoking", None),
                  outcomes.get("Hypertension", None),
                  outcomes.get("Asthma", None),
                  outcomes.get("Obesity", None),
                  ]
            donuts = [x for x in donuts if x]
            donuts = utils.pad_dashboard(donuts)
            cards = donuts
            if cards[-3:] == [None, None, None]:
                cards = cards[:-3]
            sae_outcomes = utils.nest(cards)
        except (AttributeError, TypeError):
            sae_outcomes = {}

        return render(request, 'analysis/home.html',
            {"landing": True, 
            "sae": True,
            "sae_landing": True,
            "outcome_form": outcome_form, 
            "filter_form": filter_form, 
            "geo_form":geo_form,
            "outcomes": sae_outcomes,
            "open_modal": this_open_modal,
            })
    elif settings.TWO_FACTOR_ENABLED:
        return redirect('two_factor:setup')

@login_required
def home(request, open_modal=None):
    this_open_modal = True if open_modal else False
    if not settings.TWO_FACTOR_ENABLED or request.user.is_verified():
        outcome_form, filter_form, geo_form = forms.process_queryform(request)

        is_auth = request.user.groups.filter(name__in=settings.AUTH_GROUPS).exists()
        values = SummaryOutcome.current.all()
        outcomes = {}
        try:
            for v in values:
                if v.field in DASHBOARD_CONDITIONS:
                    db_outcome = DASHBOARD_CONDITIONS[v.field]['outcome']
                    auth_outcome = False
                    for outcome in settings.AUTH_OUTCOMES:
                        if outcome in db_outcome:
                            auth_outcome = True
                    if not is_auth and auth_outcome:
                        continue
        
                try:
                    outcome = {'name': v.field,
                           'display_name': v.display_name,
                           'interest': v.values_of_interest,
                           'value': v.this_month,
                           'trendline': v.trendline,
                           'chart_type': v.chart_type
                           }
                    outcomes[v.field] = outcome
                except TypeError:
                    pass

            donuts = [outcomes.get("Type 2 Diabetes", None),
                  outcomes.get("Smoking", None),
                  outcomes.get("Hypertension", None),
                  outcomes.get("Pediatric Asthma", None),
                  outcomes.get("Obesity", None),
                  outcomes.get("Overweight", None),
                  outcomes.get("Curr ILI vac", None),
                  outcomes.get("Depression", None),
                  outcomes.get("Opioid Rx", None),
                  outcomes.get("Syphilis Test Pregnant", None),
                  outcomes.get("Hepc Boomer", None),
                  outcomes.get("Young Female Chlamy", None)]
            donuts = [x for x in donuts if x]
            donuts = utils.pad_dashboard(donuts)
            sparks = [outcomes.get("Influenza like illness Monthly", None),
                  outcomes.get("Lyme disease Monthly", None),
                  outcomes.get("Pertussis syndrome Monthly", None),
                  outcomes.get("Influenza like illness Cumulative", None),
                  outcomes.get("Lyme disease Cumulative", None),
                  outcomes.get("Pertussis syndrome Cumulative", None)]
            sparks = [x for x in sparks if x]
            sparks = utils.pad_dashboard(sparks)

            cards = donuts + sparks
            if cards[-3:] == [None, None, None]:
                cards = cards[:-3]
            rs_outcomes = utils.nest(cards)
        except (AttributeError, TypeError):
            rs_outcomes = {}

        return render(request, 'analysis/home.html',
            {"landing": True, 
            "outcome_form": outcome_form, 
            "filter_form": filter_form, 
            "geo_form":geo_form,
            "outcomes":rs_outcomes,
            "open_modal": this_open_modal,
            })

    elif settings.TWO_FACTOR_ENABLED:
        return redirect('two_factor:setup')

@login_required
def about(request):
    outcome_form, filter_form, geo_form = forms.process_queryform(request)
    context = {"about": True}
    if outcome_form is not None:
        if outcome_form.is_valid() and filter_form.is_valid():
            pass
        context['outcome_form'] = outcome_form
        context['filter_form'] = filter_form
    return TemplateResponse(request, 'analysis/about.html', context)

@login_required
@conditional_decorator(otp_required,settings.TWO_FACTOR_ENABLED)
def sae_obs_heat_map(request):
    if 'sae' in request.META['HTTP_REFERER']:
        outcome_form, filter_form, geo_form = forms.get_obs_queryforms(request)
        year = None
        month = None
        dynamic_start = None
        dynamic_end = None
    else:
        chunked = unpack_params(request, 'chunked', 'month')
        sby = unpack_params(request, 'sby', None)
        sbm = unpack_params(request, 'sbm', None)
        eby = unpack_params(request, 'eby', None)
        ebm = unpack_params(request, 'ebm', None)
        default_end_year = unpack_params(request, 'ey', None)
        default_end_month = unpack_params(request, 'em', None)
        update_time = (eby != default_end_year) or (ebm != default_end_month)
        year = eby if update_time else None
        month = ebm if update_time else None
        dynamic_start = None
        dynamic_end = None
        if sby is not None:
            dynamic_start = {'quarter': unpack_params(request, 'sbq', None),
                                    'month': sbm,
                                    'year': sby}
            dynamic_end = {'quarter': unpack_params(request, 'ebq', None),
                                  'month': unpack_params(request, 'ebm', None),
                                  'year': unpack_params(request, 'eby', None)}

        outcome_form, filter_form, geo_form = forms.process_queryform(request)
    geo_area = geo_form.cleaned_data['geo_area'] if geo_form.is_valid() else settings.GEO_AREA_DEFAULT
    geo_place_type = None
    if outcome_form is None:
        return redirect(reverse('home'))
    if outcome_form.is_valid() and filter_form.is_valid() and geo_form.is_valid():
        pass
    mrc = MapResultCache.objects.get_cached_results(outcome_form=outcome_form, 
                                                    filter_form=filter_form,
                                                    geo_form=geo_form,
                                                    month=month,
                                                    year=year)
    if mrc is None or mrc.result is None:
        mrc = MapResultCache.objects.cache_results(outcome_form=outcome_form, 
                                                    filter_form=filter_form, 
                                                    geo_form= geo_form,
                                                    result=None,
                                                    month=month,
                                                    year=year)
        jobs.generate_map_cache.delay(mrc.pk, geo_area, month, year)

    return TemplateResponse(request, 'analysis/map.html',
                            {"map": True,
                             "map_cache": mrc.pk,
                             "outcome_form": outcome_form,
                             "filter_form": filter_form,
                             "geo_form":geo_form,
                             "geo_area":geo_area,
                             "geo_place_type":geo_place_type,
                             "chunked": 'month',
                             "dynamic_start":dynamic_start,
                             "dynamic_end":dynamic_end})

@login_required
@conditional_decorator(otp_required,settings.TWO_FACTOR_ENABLED)
def sae_heat_map(request, outcome=None):
    chunked = unpack_params(request, 'chunked', 'month')
    sby = unpack_params(request, 'sby', None)
    sbm = unpack_params(request, 'sbm', None)
    eby = unpack_params(request, 'eby', None)
    ebm = unpack_params(request, 'ebm', None)
    default_end_year = unpack_params(request, 'ey', None)
    default_end_month = unpack_params(request, 'em', None)
    update_time = (eby != default_end_year)
    year = eby if update_time else None
    month = ebm if update_time else None
    month_range, year_range, start, end = get_time_range('zip')
    if year is None:
        year = end.year

    dynamic_start = None
    dynamic_end = None
    if sby is not None:
        dynamic_start = {'quarter': unpack_params(request, 'sbq', None),
                                    'month': sbm,
                                    'year': sby}
        dynamic_end = {'quarter': unpack_params(request, 'ebq', None),
                                  'month': unpack_params(request, 'ebm', None),
                                  'year': unpack_params(request, 'eby', None)}

    outcome_form, filter_form, geo_form = forms.process_sae_queryform(request, outcome)
    geo_area = geo_form.cleaned_data['geo_area'] if geo_form.is_valid() else 'zip'
    geo_type = settings.GEO_PLACES[geo_area]['GEO_AREA'] if settings.GEO_PLACES else None
    if outcome_form is None:
        return redirect(reverse('sae_home'))
    if outcome_form.is_valid() and filter_form.is_valid() and geo_form.is_valid():
        pass

    mrc = MapSAECache.objects.get_cached_results(outcome_form=outcome_form, 
                                                    filter_form=filter_form,
                                                    geo_form=geo_form,
                                                    year=year)
    if mrc is None or mrc.result is None:
        mrc = MapSAECache.objects.cache_results(outcome_form=outcome_form, 
                                                    filter_form=filter_form, 
                                                    geo_form= geo_form,
                                                    result=None,
                                                    year=year)
        jobs.generate_sae_map_cache.delay(mrc.pk, geo_area, year)

    return TemplateResponse(request, 'analysis/map.html',
                            {"map": True,
                             "map_cache": mrc.pk,
                             "outcome_form": outcome_form,
                             "filter_form": filter_form,
                             "geo_form":geo_form,
                             "geo_area":geo_area,
                             "geo_type":geo_type,
                             "chunked": chunked,
                             "dynamic_start":dynamic_start,
                             "dynamic_end":dynamic_end,
                             "sae": True,
                             })



@login_required
@conditional_decorator(otp_required,settings.TWO_FACTOR_ENABLED)
def heat_map(request, outcome=None):
    chunked = unpack_params(request, 'chunked', 'month')
    sby = unpack_params(request, 'sby', None)
    sbm = unpack_params(request, 'sbm', None)
    eby = unpack_params(request, 'eby', None)
    ebm = unpack_params(request, 'ebm', None)
    default_end_year = unpack_params(request, 'ey', None)
    default_end_month = unpack_params(request, 'em', None)
    update_time = (eby != default_end_year) or (ebm != default_end_month)
    year = eby if update_time else None
    month = ebm if update_time else None
    dynamic_start = None
    dynamic_end = None
    if sby is not None:
        dynamic_start = {'quarter': unpack_params(request, 'sbq', None),
                                    'month': sbm,
                                    'year': sby}
        dynamic_end = {'quarter': unpack_params(request, 'ebq', None),
                                  'month': unpack_params(request, 'ebm', None),
                                  'year': unpack_params(request, 'eby', None)}

    outcome_form, filter_form, geo_form = forms.process_queryform(request, outcome)
    geo_area = geo_form.cleaned_data['geo_area'] if geo_form.is_valid() else settings.GEO_AREA_DEFAULT
    geo_place_type = settings.GEO_PLACES[geo_area]['GEO_AREA'] if settings.GEO_PLACES else None
    if outcome_form is None:
        return redirect(reverse('home'))
    if outcome_form.is_valid() and filter_form.is_valid() and geo_form.is_valid():
        pass
    mrc = MapResultCache.objects.get_cached_results(outcome_form=outcome_form, 
                                                    filter_form=filter_form,
                                                    geo_form=geo_form,
                                                    month=month,
                                                    year=year)
    if mrc is None or mrc.result is None:
        mrc = MapResultCache.objects.cache_results(outcome_form=outcome_form, 
                                                    filter_form=filter_form, 
                                                    geo_form= geo_form,
                                                    result=None,
                                                    month=month,
                                                    year=year)
        jobs.generate_map_cache.delay(mrc.pk, geo_area, month, year)

    return TemplateResponse(request, 'analysis/map.html',
                            {"map": True,
                             "map_cache": mrc.pk,
                             "outcome_form": outcome_form,
                             "filter_form": filter_form,
                             "geo_form":geo_form,
                             "geo_area":geo_area,
                             "geo_place_type":geo_place_type,
                             "chunked": chunked,
                             "dynamic_start":dynamic_start,
                             "dynamic_end":dynamic_end})


@login_required
def map_overlay(request, pk):
    if request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest':
        if request.method == 'GET':
            mrc = get_object_or_404(MapResultCache, pk=pk)
            if mrc.result is not None:
                return JsonResponse({"complete": True, "data": mrc.result})
            else:
                return JsonResponse({"complete": False})
        else:
            return JsonResponse(status=405)
    else:
        return JsonResponse(status=400)


@login_required
def sae_map_overlay(request, pk):
    if request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest':
        if request.method == 'GET':
            mrc = get_object_or_404(MapSAECache, pk=pk)
            if mrc.result is not None:
                return JsonResponse({"complete": True, "data": mrc.result})
            else:
                return JsonResponse({"complete": False})
        else:
            return JsonResponse(status=405)
    else:
        return JsonResponse(status=400)

@login_required
@conditional_decorator(otp_required,settings.TWO_FACTOR_ENABLED)
def graph_view(request, graph_type, outcome=None):
    outcome_form, filter_form, geo_form = forms.process_queryform(request, outcome)
    geo_area = geo_form.cleaned_data['geo_area'] if geo_form.is_valid() else settings.GEO_AREA_DEFAULT
    geo_place_type = settings.GEO_PLACES[geo_area]['GEO_AREA'] if settings.GEO_PLACES else None
    if outcome_form is None:
        return redirect(reverse('home'))
    if outcome_form.is_valid() and filter_form.is_valid() and geo_form.is_valid():
        pass

    if geo_area == 'zip' or geo_area == 'tract':
        geo_location = settings.GEO_LOCATION
    else:
        geo_location = settings.GEO_PLACES[geo_area]['GEO_LOCATION']
    default = geo_location + ',' + geo_location

    data = [{'location': ''}, {'location': ''}]
    locations = unpack_params(request, 'locations', default).split(',')
    data[0]['location'] = locations[0] if locations[0] != 'undefined' else geo_location
    try:
        data[1]['location'] = locations[1] if locations[1] != 'undefined' else geo_location
    except IndexError:
        data[1]['location'] = locations[0] if locations[0] != 'undefined' else geo_location
    
    stratify = unpack_params(request, 'stratify',
                             'age_group' if graph_type == "current" else 'none')
    if stratify == 'undefined': 
        stratify = 'age_group'
    elif graph_type == "current" and stratify == 'none': 
        stratify = 'age_group'
    stratify2 = unpack_params(request, 'stratify2', 'none')
    if stratify2 == 'undefined': stratify2 = 'none'

    chunked = unpack_params(request, 'chunked', 'month')


    context = {"stratify": stratify, 
               "stratify2": stratify2,
               "outcome_form": outcome_form, 
               "filter_form": filter_form,
               "geo_form": geo_form,
               "geo_area":geo_area,
               "geo_place_type":geo_place_type,
               "geo_location":geo_location}

    sby = unpack_params(request, 'sby', None)
    sbm = unpack_params(request, 'sbm', None)
    eby = unpack_params(request, 'eby', None)
    ebm = unpack_params(request, 'ebm', None)
    default_end_year = unpack_params(request, 'ey', None)
    default_end_month = unpack_params(request, 'em', None)
    update_time = (eby != default_end_year) or (ebm != default_end_month)
    year = eby if update_time else None
    month = ebm if update_time else None
    if sby is not None:
        context['dynamic_start'] = {'quarter': unpack_params(request, 'sbq', None),
                                    'month': sbm,
                                    'year': sby}
        context['dynamic_end'] = {'quarter': unpack_params(request, 'ebq', None),
                                  'month': unpack_params(request, 'ebm', None),
                                  'year': unpack_params(request, 'eby', None)}

    try:
        if graph_type == "current":
            context["current"] = True
            context['data'] = current_rates(data, outcome_form, filter_form, geo_form, 
                                            stratify, stratify2, month, year)
            context['chunked'] = chunked
            template = 'analysis/current.html'
        elif graph_type == "timeseries":
            context['timeseries'] = True
            context['data'] = timeseries(data, outcome_form, filter_form, geo_form, stratify, chunked)
            context['chunked'] = chunked

            template = 'analysis/timeseries.html'
        else:
            return redirect(reverse('home'))
    except AttributeError as e:
        return redirect(reverse('home'))

    tr = TemplateResponse(request, template, context)

    return tr

@login_required
@conditional_decorator(otp_required,settings.TWO_FACTOR_ENABLED)
def sae_obs_graph_view(request, graph_type):
    if 'sae' in request.META['HTTP_REFERER']:
        # get stratification values from sae forms
        sae_outcome_form, sae_filter_form, sae_geo_form = forms.process_sae_queryform(request, None)
        sae_strat1 = sae_filter_form.cleaned_data['demographic1'] if sae_filter_form.is_valid() else 'overall'
        sae_strat2 = sae_filter_form.cleaned_data['demographic2'] if sae_filter_form.is_valid() else 'overall'
        sae_obs_strat = {
            'overall': 'none',
            'age_group': 'age_group',
            'gender': 'sex',
            'race': 'race_ethnicity',
            }
        stratify = sae_obs_strat[sae_strat1]
        stratify2 = sae_obs_strat[sae_strat2]
        # get obs forms
        outcome_form, filter_form, geo_form = forms.get_obs_queryforms(request, True)
        year = None
        month = None
        dynamic_start = {'quarter': None,
                                    'month': None,
                                    'year': None}
        dynamic_end = {'quarter': None,
                                  'month': None,
                                  'year': None}
    else:
        outcome_form, filter_form, geo_form = forms.process_queryform(request)
        stratify = unpack_params(request, 'stratify',
                             'age_group' if graph_type == "current" else 'none')
        stratify2 = unpack_params(request, 'stratify2', 'none')
        sby = unpack_params(request, 'sby', None)
        sbm = unpack_params(request, 'sbm', None)
        eby = unpack_params(request, 'eby', None)
        ebm = unpack_params(request, 'ebm', None)
        default_end_year = unpack_params(request, 'ey', None)
        default_end_month = unpack_params(request, 'em', None)
        update_time = (eby != default_end_year) or (ebm != default_end_month)
        year = eby if update_time else None
        month = ebm if update_time else None
        dynamic_start = None
        dynamic_end = None
        if sby is not None:
            dynamic_start = {'quarter': unpack_params(request, 'sbq', None),
                                    'month': sbm,
                                    'year': sby}
            dynamic_end = {'quarter': unpack_params(request, 'ebq', None),
                                  'month': unpack_params(request, 'ebm', None),
                                  'year': unpack_params(request, 'eby', None)}



    geo_area = geo_form.cleaned_data['geo_area'] if geo_form.is_valid() else settings.GEO_AREA_DEFAULT
    geo_place_type = None 
    if outcome_form is None:
        return redirect(reverse('home'))
    if outcome_form.is_valid() and filter_form.is_valid() and geo_form.is_valid():
        pass

    geo_location = settings.GEO_LOCATION
    default = geo_location + ',' + geo_location

    data = [{'location': ''}, {'location': ''}]
    locations = unpack_params(request, 'locations', default).split(',')
    data[0]['location'] = locations[0]
    try:
        data[1]['location'] = locations[1]
    except IndexError:
        data[1]['location'] = locations[0]
    chunked = 'month'
    context = {"stratify": stratify, 
               "stratify2": stratify2,
               "outcome_form": outcome_form, 
               "filter_form": filter_form,
               "geo_form": geo_form,
               "geo_area":geo_area,
               "geo_place_type":geo_place_type,
               "geo_location":geo_location,
               "chunked": chunked,
               "dynamic_start": dynamic_start,
               "dynamci_end": dynamic_end}

    try:
        if graph_type == "current":
            context["current"] = True
            context['data'] = current_rates(data, outcome_form, filter_form, geo_form, 
                                            stratify, stratify2, month, year)
            template = 'analysis/current.html'
        elif graph_type == "timeseries":
            context['timeseries'] = True
            context['data'] = timeseries(data, outcome_form, filter_form, geo_form, stratify, chunked)
            template = 'analysis/timeseries.html'
        else:
            return redirect(reverse('home'))
    except AttributeError as e:
        return redirect(reverse('home'))

    tr = TemplateResponse(request, template, context)

    return tr


@login_required
@conditional_decorator(otp_required,settings.TWO_FACTOR_ENABLED)
def sae_graph_view(request, graph_type, outcome=None):
    outcome_form, filter_form, geo_form = forms.process_sae_queryform(request, outcome)
    geo_area = geo_form.cleaned_data['geo_area'] if geo_form.is_valid() else 'zip'
    if outcome_form is None:
        return redirect(reverse('sae_map'))
    if outcome_form.is_valid() and filter_form.is_valid() and geo_form.is_valid():
        pass

    geo_location = settings.GEO_LOCATION
    default = geo_location + ',' + geo_location

    data = [{'location': '','location_label':''}, {'location': '','location_label':''}]
    locations = unpack_params(request, 'locations', default).split(',')
    data[0]['location'] = locations[0] if locations[0] != 'undefined' else geo_location
    data[0]['location_label'] = locations[0] if locations[0] != 'undefined' else geo_location
    try:
        data[1]['location'] = locations[1] if locations[1] != 'undefined' else geo_location
        data[1]['location_label'] = locations[1] if locations[1] != 'undefined' else geo_location
    except IndexError:
        data[1]['location'] = locations[0] if locations[0] != 'undefined' else geo_location
        data[1]['location_label'] = locations[0] if locations[0] != 'undefined' else geo_location

    stratify1 = filter_form.cleaned_data['demographic1'] if filter_form.is_valid() else 'overall'
    stratify2 = filter_form.cleaned_data['demographic2'] if filter_form.is_valid() else 'overall'

    zips = ZipLevel.objects.all().order_by('zip').distinct('zip').values_list('zip', flat=True)
    munis = MuniLevel.objects.all().order_by('muni').distinct('muni').values_list('muni', flat=True)

    location_list = {
                     "zips": zips,
                     "munis": munis,
                     }

    if graph_type == "current" and stratify1 == 'overall':
        stratify1 = 'age_group'
        stratify2 = 'overall'
    context = {"stratify": stratify1, 
               "stratify2": stratify2,
               "outcome_form": outcome_form, 
               "filter_form": filter_form,
               "geo_form": geo_form,
               "geo_area":geo_area,
               "geo_location":geo_location,
               "static_stratifications": None, 
               "location_list": location_list,
               "sae": True,
               }

    #chunked = unpack_params(request, 'chunked', 'year')
    chunked = unpack_params(request, 'chunked', 'month')
    sby = unpack_params(request, 'sby', None)
    sbm = unpack_params(request, 'sbm', None)
    eby = unpack_params(request, 'eby', None)
    ebm = unpack_params(request, 'ebm', None)
    default_end_year = unpack_params(request, 'ey', None)
    default_end_month = unpack_params(request, 'em', None)
    update_time = (eby != default_end_year)
    year = eby if update_time else None #default_end_year
    month = ebm if update_time else None #default_end_month
    month_range, year_range, start, end = get_time_range('zip')
    if year is None:
        year = end.year
    if sby is not None:
        context['dynamic_start'] = {'quarter': unpack_params(request, 'sbq', None),
                                    'month': sbm,
                                    'year': sby}
        context['dynamic_end'] = {'quarter': unpack_params(request, 'ebq', None),
                                  'month': unpack_params(request, 'ebm', None),
                                  'year': unpack_params(request, 'eby', None)}

    try:
        if graph_type == "current":
            context["current"] = True
            context['data'] = sae_current_rates(data, outcome_form, filter_form, geo_form, 
                                                stratify1, stratify2, year)
            context['chunked'] = chunked
            template = 'analysis/sae-current.html'
        elif graph_type == "timeseries":
            context['timeseries'] = True
            context['data'] = sae_timeseries(data, outcome_form, filter_form, geo_form, stratify1, stratify2)
            context['chunked'] = chunked
            template = 'analysis/sae-timeseries.html'
        else:
            return redirect(reverse('sae_map'))
    except AttributeError as e:
        return redirect(reverse('sae_map'))

    tr = TemplateResponse(request, template, context)

    return tr

@login_required
def current_graph(request, pk1, pk2):
    return _graph_view(request, pk1, pk2, CurrentRateResultCache)

@login_required
def sae_current_graph(request, pk1, pk2):
    return _sae_graph_view(request, pk1, pk2, CurrentRateSAECache)

@login_required
def timeseries_graph(request, pk1, pk2):
    return _graph_view(request, pk1, pk2, TimeseriesResultCache)

@login_required
def sae_timeseries_graph(request, pk1, pk2):
    return _sae_graph_view(request, pk1, pk2, TimeseriesSAECache)

@login_required
def timeseries_trend(request, inflection, pk1, pk2):
    return _graph_view(request, pk1, pk2, TimeseriesResultCache, inflection)

@login_required
def sae_timeseries_trend(request, inflection, pk1, pk2):
    return _sae_graph_view(request, pk1, pk2, TimeseriesSAECache, inflection)

@login_required
def user_report_csv(request):
    if not request.user.is_superuser:
        return HttpResponseBadRequest('')
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(
    content_type='text/csv',
    headers={'Content-Disposition': 'attachment; filename="user_report.csv"'},
    )
    # write csv header
    writer = csv.writer(response)
    writer.writerow(['full name','institution','email', 'active', 'staff', 'superuser', 'last login date',])
    # write user info
    User = get_user_model()
    users = User.objects.all()
    for u in users:
        p = Profile.objects.get(user=u)
        user_last_login_date = u.last_login.strftime("%Y-%m-%d") if u.last_login else None
        if u.username:
            writer.writerow([
                        p.full_name,
                        p.institution,
                        u.email,
                        u.is_active,
                        u.is_staff,
                        u.is_superuser,
                        user_last_login_date,
                        ])

    # write download date
    writer.writerow([])
    now = datetime.now().strftime("%Y-%m-%d")
    writer.writerow(['Download Date: ', now])

    return response


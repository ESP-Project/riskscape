from django.test import TestCase
from analysis.models import SAESummaryOutcome
from analysis.models import SummaryOutcome
import pdb

class ContextProc(TestCase):
    def setUp(self):
        SAESummaryOutcome.objects.create(field="Diabetes",chart_type='D')
        #SummaryOutcome.objects.create(field="Smoking",chart_type='D')

    def test_sae_outcome(self):

        values = SAESummaryOutcome.current.all()
        outcomes = {}
        
        for v in values:
            outcome = {'name': v.field,
                        'display_name': v.display_name,
                           'interest': v.values_of_interest,
                           'value': v.this_month,
                           'trendline': v.trendline,
                           'chart_type': v.chart_type
                           }
            outcomes[v.field] = outcome

            self.assertEqual(v.field, 'Diabetes')
    '''
    def test_outcome(self):

        values = SummaryOutcome.current.all()
        outcomes = {}

        pdb.set_trace()
        for v in values:
            outcome = {'name': v.field,
                        'display_name': v.display_name,
                           'interest': v.values_of_interest,
                           'value': v.this_month,
                           'trendline': v.trendline,
                           'chart_type': v.chart_type
                           }
            outcomes[v.field] = outcome

            self.assertEqual(v.field, 'Smoking')
    '''

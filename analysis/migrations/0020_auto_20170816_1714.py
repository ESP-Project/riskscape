# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0019_auto_20170308_1257'),
    ]

    operations = [
        migrations.AlterField(
            model_name='city',
            name='city',
            field=models.CharField(max_length=32, serialize=False, primary_key=True),
        ),
        migrations.RunSQL('ALTER TABLE analysis_city_zips ALTER city_id TYPE varchar(32);'),
    ]

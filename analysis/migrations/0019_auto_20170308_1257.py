# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0018_auto_20170118_0958'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='summaryoutcome',
            name='least_recent_val',
        ),
        migrations.RemoveField(
            model_name='summaryoutcome',
            name='most_recent_val',
        ),
        migrations.RemoveField(
            model_name='summaryoutcome',
            name='next_most_recent_val',
        ),
        migrations.RemoveField(
            model_name='summaryoutcome',
            name='value',
        ),
    ]

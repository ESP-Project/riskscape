# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0012_timeseriesresultcache_viewed'),
    ]

    operations = [
        migrations.CreateModel(
            name='MapResultCache',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('outcome', models.JSONField(default=dict)),
                ('filter', models.JSONField(default=dict)),
                ('result', models.JSONField(null=True, blank=True)),
                ('viewed', models.IntegerField(default=1)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='mapresultcache',
            unique_together=set([('outcome', 'filter')]),
        ),
    ]

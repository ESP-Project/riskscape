# Generated by Django 2.1.15 on 2020-07-22 17:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0003_auto_20200624_1815'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='currentrateresultcache',
            unique_together={('outcome_hash', 'filter_hash', 'location', 'stratify', 'geo_hash')},
        ),
        migrations.AlterUniqueTogether(
            name='timeseriesresultcache',
            unique_together={('outcome_hash', 'filter_hash', 'location', 'stratify', 'time_chunk', 'geo_hash')},
        ),
    ]

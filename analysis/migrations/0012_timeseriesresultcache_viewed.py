# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0011_auto_20161020_1229'),
    ]

    operations = [
        migrations.AddField(
            model_name='timeseriesresultcache',
            name='viewed',
            field=models.IntegerField(default=1),
        ),
    ]

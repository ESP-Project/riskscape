# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0015_auto_20170113_0757'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='summaryoutcome',
            name='filter',
        ),
        migrations.RemoveField(
            model_name='summaryoutcome',
            name='outcome',
        ),
    ]

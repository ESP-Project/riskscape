# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0020_auto_20170816_1714'),
    ]

    operations = [
        migrations.AlterField(
            model_name='currentrateresultcache',
            name='location',
            field=models.CharField(max_length=32),
        ),
        migrations.AlterField(
            model_name='timeseriesresultcache',
            name='location',
            field=models.CharField(max_length=32),
        ),
    ]

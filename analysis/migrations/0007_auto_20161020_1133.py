# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0006_auto_20161020_1130'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='resultcache',
            unique_together=set([('outcome', 'filter', 'interface')]),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0016_auto_20170116_0808'),
    ]

    operations = [
        migrations.RenameField(
            model_name='currentrateresultcache',
            old_name='filter',
            new_name='filter_form',
        ),
        migrations.RenameField(
            model_name='currentrateresultcache',
            old_name='outcome',
            new_name='outcome_form',
        ),
        migrations.RenameField(
            model_name='mapresultcache',
            old_name='filter',
            new_name='filter_form',
        ),
        migrations.RenameField(
            model_name='mapresultcache',
            old_name='outcome',
            new_name='outcome_form',
        ),
        migrations.RenameField(
            model_name='timeseriesresultcache',
            old_name='filter',
            new_name='filter_form',
        ),
        migrations.RenameField(
            model_name='timeseriesresultcache',
            old_name='outcome',
            new_name='outcome_form',
        ),
        migrations.AlterUniqueTogether(
            name='currentrateresultcache',
            unique_together=set([('outcome_form', 'filter_form', 'location', 'stratify')]),
        ),
        migrations.AlterUniqueTogether(
            name='mapresultcache',
            unique_together=set([('outcome_form', 'filter_form')]),
        ),
        migrations.AlterUniqueTogether(
            name='timeseriesresultcache',
            unique_together=set([('outcome_form', 'filter_form', 'location', 'stratify', 'time_chunk')]),
        ),
    ]

# -*- coding: utf-8 -*-
from django.db import migrations
from django.core import management

class Migration(migrations.Migration):
    dependencies = [
        ('analysis', '0001_squashed_0029_auto_20190510_1432'),
    ]

    operations = [
    ]


def populate_cities(apps, schema_editor):
    # cities and zip codes.
    management.call_command('load_census', '--cityzip')

def reverse_func_city(apps, schema_editor):
    # forwards_func() creates two Country instances,
    # so reverse_func() should delete them.
    City = apps.get_model("analysis", "City")
    City.objects.all().delete()
    Census = apps.get_model("analysis", "Census")
    City.objects.all().delete()

def populate_census(apps, schema_editor):
    # zip codes and population.
    management.call_command('load_census', '--zip')


def reverse_func_census(apps, schema_editor):
    # forwards_func() creates two Country instances,
    # so reverse_func() should delete them.
    Census = apps.get_model("analysis", "Census")
    Census.objects.update(population=None)

def update_coverage(apps, schema_editor):
    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    Census = apps.get_model("analysis", "Census")
    for census in Census.objects.all():
        census.save()

def populate_neigborhoods(apps, schema_editor):
    # not used: include neighborhoods in populate_cities.
    pass

def reverse_func_neighborhoods(apps, schema_editor):
    # not used
    pass

def clear_cache(apps, schema_editor):
    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    map = apps.get_model('analysis', 'MapResultCache')
    map.objects.all().delete()
    current = apps.get_model('analysis', 'CurrentRateResultCache')
    current.objects.all().delete()
    timeseries = apps.get_model('analysis', 'TimeseriesResultCache')
    timeseries.objects.all().delete()


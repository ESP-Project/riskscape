# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0008_resultcache_stratify'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='resultcache',
            unique_together=set([('outcome', 'filter', 'interface', 'stratify')]),
        ),
    ]

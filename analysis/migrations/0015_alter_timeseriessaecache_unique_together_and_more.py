# Generated by Django 4.2 on 2023-10-31 16:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0014_remove_currentratesaecache_month_and_more'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='timeseriessaecache',
            unique_together=set(),
        ),
        migrations.AddField(
            model_name='timeseriessaecache',
            name='stratify2',
            field=models.CharField(blank=True, max_length=32, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='timeseriessaecache',
            unique_together={('outcome_hash', 'filter_hash', 'location', 'stratify', 'stratify2', 'geo_hash')},
        ),
    ]

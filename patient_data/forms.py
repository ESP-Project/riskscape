from django import forms
from patient_data.models import UploadData, Uploadfile


class UploadRawDataForm(forms.ModelForm):
    class Meta:
        model = UploadData
        fields = ['data_file', 'site_name', 'data_type']

class UploadFileForm(forms.ModelForm):
    class Meta:
        model = Uploadfile
        fields = ['description','document']

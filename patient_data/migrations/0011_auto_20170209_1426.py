# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0010_auto_20170209_0832'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uploaddata',
            name='status',
            field=models.IntegerField(choices=[(0, 'Pending'), (1, 'Running'), (2, 'Success'), (3, 'Failure')], default=0),
        ),
    ]

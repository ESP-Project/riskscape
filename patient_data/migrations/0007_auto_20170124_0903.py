# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0006_auto_20161221_0936'),
    ]

    operations = [
        migrations.AlterField(
            model_name='riskscapedata',
            name='site',
            field=models.CharField(choices=[('ATR', 'Atrius'), ('CHA', 'CHA'), ('ML', 'Mass League')], blank=True, max_length=3, null=True, verbose_name='Practice Group'),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='site',
            field=models.CharField(choices=[('ATR', 'Atrius'), ('CHA', 'CHA'), ('ML', 'Mass League')], blank=True, max_length=3, null=True, verbose_name='Practice Group'),
        ),
    ]

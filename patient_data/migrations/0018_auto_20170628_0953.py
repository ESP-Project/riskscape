# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0017_auto_20170519_1135'),
    ]

    operations = [
        migrations.AddField(
            model_name='riskscapedata',
            name='pertussis',
            field=models.PositiveSmallIntegerField(verbose_name='Pertussis syndrome', null=True, blank=True, choices=[(1, 'Yes'), (0, 'No')]),
        ),
        migrations.AddField(
            model_name='riskscapedata',
            name='pertussis_last',
            field=models.PositiveSmallIntegerField(verbose_name='Pertussis syndrome (last year)', null=True, blank=True, choices=[(1, 'Yes'), (0, 'No')]),
        ),
        migrations.AddField(
            model_name='trimtrackerdata',
            name='pertussis',
            field=models.PositiveSmallIntegerField(verbose_name='Pertussis syndrome', null=True, blank=True, choices=[(1, 'Yes'), (0, 'No')]),
        ),
        migrations.AddField(
            model_name='trimtrackerdata',
            name='pertussis_last',
            field=models.PositiveSmallIntegerField(verbose_name='Pertussis syndrome (last year)', null=True, blank=True, choices=[(1, 'Yes'), (0, 'No')]),
        ),
        migrations.AlterField(
            model_name='riskscapedata',
            name='chlamydia',
            field=models.PositiveSmallIntegerField(verbose_name='Chlamydia', null=True, blank=True, choices=[(0, 'No Test'), (1, 'Indeterminate'), (2, 'Negative'), (3, 'Positive')]),
        ),
        migrations.AlterField(
            model_name='riskscapedata',
            name='gonorrhea',
            field=models.PositiveSmallIntegerField(verbose_name='Gonorrhea Test', null=True, blank=True, choices=[(0, 'No Test'), (1, 'Indeterminate'), (2, 'Negative'), (3, 'Positive')]),
        ),
        migrations.AlterField(
            model_name='riskscapedata',
            name='syph_test',
            field=models.PositiveSmallIntegerField(verbose_name='Syphilis Screen', null=True, blank=True, choices=[(0, 'No Test'), (1, '0-1 years ago'), (2, '1-2 years ago'), (3, '&gt;2 years ago')]),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='chlamydia',
            field=models.PositiveSmallIntegerField(verbose_name='Chlamydia', null=True, blank=True, choices=[(0, 'No Test'), (1, 'Indeterminate'), (2, 'Negative'), (3, 'Positive')]),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='gonorrhea',
            field=models.PositiveSmallIntegerField(verbose_name='Gonorrhea Test', null=True, blank=True, choices=[(0, 'No Test'), (1, 'Indeterminate'), (2, 'Negative'), (3, 'Positive')]),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='syph_test',
            field=models.PositiveSmallIntegerField(verbose_name='Syphilis Screen', null=True, blank=True, choices=[(0, 'No Test'), (1, '0-1 years ago'), (2, '1-2 years ago'), (3, '&gt;2 years ago')]),
        ),
    ]

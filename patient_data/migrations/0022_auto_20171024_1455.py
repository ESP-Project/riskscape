# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0021_auto_20170829_1556'),
    ]

    operations = [
        migrations.AlterField(
            model_name='riskscapedata',
            name='depression',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Yes'), (0, 'No')], null=True, verbose_name='Depression (Rx)', blank=True),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='depression',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Yes'), (0, 'No')], null=True, verbose_name='Depression (Rx)', blank=True),
        ),
    ]

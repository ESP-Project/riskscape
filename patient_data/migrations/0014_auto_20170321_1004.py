# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0013_auto_20170308_1742'),
    ]

    operations = [
        migrations.AlterField(
            model_name='riskscapedata',
            name='ili_current',
            field=models.PositiveSmallIntegerField(null=True, verbose_name='Seasonal ILI (monthly)', choices=[(1, 'Yes'), (0, 'No')], blank=True),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='ili_current',
            field=models.PositiveSmallIntegerField(null=True, verbose_name='Seasonal ILI (monthly)', choices=[(1, 'Yes'), (0, 'No')], blank=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import patient_data.models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0005_auto_20161208_1130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uploaddata',
            name='data_file',
            field=models.FileField(upload_to=patient_data.models.path_and_rename),
        ),
    ]

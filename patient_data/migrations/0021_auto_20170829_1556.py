# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0020_auto_20170823_1418'),
    ]

    operations = [
        migrations.AlterField(
            model_name='riskscapedata',
            name='pertussis',
            field=models.PositiveSmallIntegerField(null=True, verbose_name='Pertussis syndrome (monthly)', blank=True, choices=[(1, 'Yes'), (0, 'No')]),
        ),
        migrations.AlterField(
            model_name='riskscapedata',
            name='pertussis_last',
            field=models.PositiveSmallIntegerField(null=True, verbose_name='Pertussis syndrome cumulative last 12 months', blank=True, choices=[(1, 'Yes'), (0, 'No')]),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='pertussis',
            field=models.PositiveSmallIntegerField(null=True, verbose_name='Pertussis syndrome (monthly)', blank=True, choices=[(1, 'Yes'), (0, 'No')]),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='pertussis_last',
            field=models.PositiveSmallIntegerField(null=True, verbose_name='Pertussis syndrome cumulative last 12 months', blank=True, choices=[(1, 'Yes'), (0, 'No')]),
        ),
    ]

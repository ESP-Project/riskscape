# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0012_auto_20170209_1459'),
    ]

    operations = [
        migrations.AddField(
            model_name='riskscapedata',
            name='encounters_total',
            field=models.PositiveSmallIntegerField(null=True, default=0, verbose_name='Lifetime Encounters', blank=True),
        ),
        migrations.AddField(
            model_name='riskscapedata',
            name='lyme_last',
            field=models.PositiveSmallIntegerField(null=True, verbose_name='Lyme (last year)', choices=[(1, 'Yes'), (0, 'No')], blank=True),
        ),
        migrations.AddField(
            model_name='trimtrackerdata',
            name='encounters_total',
            field=models.PositiveSmallIntegerField(null=True, default=0, verbose_name='Lifetime Encounters', blank=True),
        ),
        migrations.AddField(
            model_name='trimtrackerdata',
            name='lyme_last',
            field=models.PositiveSmallIntegerField(null=True, verbose_name='Lyme (last year)', choices=[(1, 'Yes'), (0, 'No')], blank=True),
        ),
    ]

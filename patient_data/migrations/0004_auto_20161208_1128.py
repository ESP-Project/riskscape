# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0003_auto_20161117_0943'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uploaddata',
            name='processed',
            field=models.DateTimeField(null=True),
        ),
    ]

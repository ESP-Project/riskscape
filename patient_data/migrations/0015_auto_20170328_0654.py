# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0014_auto_20170321_1004'),
    ]

    operations = [
        migrations.AddField(
            model_name='riskscapedata',
            name='ili_cum',
            field=models.PositiveSmallIntegerField(null=True, choices=[(1, 'Yes'), (0, 'No')], verbose_name='Seasonal ILI (cumulative)', blank=True),
        ),
        migrations.AddField(
            model_name='trimtrackerdata',
            name='ili_cum',
            field=models.PositiveSmallIntegerField(null=True, choices=[(1, 'Yes'), (0, 'No')], verbose_name='Seasonal ILI (cumulative)', blank=True),
        ),
    ]

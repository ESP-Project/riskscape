# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0022_auto_20171024_1455'),
    ]

    operations = [
        migrations.AddField(
            model_name='uploaddata',
            name='is_historic',
            field=models.BooleanField(default=False),
        ),
    ]

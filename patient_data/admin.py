from django.contrib import admin

# Register your models here.
from patient_data.models import UploadData, Uploadfile

admin.site.register(UploadData)
admin.site.register(Uploadfile)

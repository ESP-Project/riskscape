# Cancels orphaned jobs in the 'upload' queue
import django_rq
from scheduler.models import CronJob
from django.core.management.base import BaseCommand

class Command(BaseCommand):

    def handle(self, *args, **options):

        # get cron jobs for the upload queue
        cron_jobs = CronJob.objects.filter(queue='upload').values_list("job_id",flat=True)

        # get scheduler for upload queue
        scheduler = django_rq.get_scheduler('upload')

        # cancel orphaned jobs: all jobs in the upload queue, except for existing cron jobs
        for s in scheduler.get_jobs():
            if s.id not in cron_jobs:
                scheduler.cancel(s.id)
                print('Canceled job id: {}'.format(s.id))
        



from django.conf import settings
from patient_data.copy_from import CopyMapping
from patient_data.models import TrimtrackerData, RiskscapeData

db_engine = settings.DATABASES['default']['ENGINE']
if db_engine in [ 'sql_server.pyodbc', 'mssql']: # ms-sql server
    from patient_data.copy_from import CopyMapping
else: # postgres
    from postgres_copy import CopyMapping

class RiskscapeCopyMapping(CopyMapping):
    def __init__(self, *args, **kwargs):
        self.full_history = False
        self.month = None
        self.year = None
        super().__init__(*args, **kwargs)

    def post_copy(self, cursor):
        if self.model is TrimtrackerData:
            options = {
                'db_table': self.temp_table_name,
            }


        if db_engine in [ 'sql_server.pyodbc', 'mssql']: # ms-sql server
            sql = """
                SELECT CAST(af AS VARCHAR(30)) af, CAST(ag AS VARCHAR(30)) ag
                FROM "%(db_table)s"
                ORDER BY CAST(af AS VARCHAR(30)) DESC, CAST(ag AS VARCHAR(30)) DESC 
                OFFSET 0 ROWS 
                FETCH NEXT 1 ROWS ONLY;
                """
        else:  # postgres
            sql = """
                SELECT af, ag FROM "%(db_table)s"
                ORDER BY af DESC, ag DESC LIMIT 1;
                """
          
        cursor.execute(sql % options)
        self.year, self.month = cursor.fetchone()

    def pre_insert(self, cursor):
        if self.model is TrimtrackerData:
            pass
        elif self.model is RiskscapeData:
            options = {
                'db_table': self.temp_table_name,
                'year': str(self.year),
                'month': str(self.month)
            }
            deletes = [""" DELETE FROM "%(db_table)s" WHERE af <> '%(year)s';""",
                       """DELETE FROM "%(db_table)s" WHERE ag <> '%(month)s';"""]

            for sql in deletes:
                cursor.execute(sql % options)

            if db_engine in [ 'sql_server.pyodbc', 'mssql']: # ms-sql server
                cursor.execute("ALTER TABLE %s DROP COLUMN af, ag;" % self.temp_table_name)
            else: # postgres
                cursor.execute("ALTER TABLE %s DROP af, DROP ag;" % self.temp_table_name)

    def save(self, **kwargs):
        with self.conn.cursor() as cursor:
            if self.model is TrimtrackerData:
                self.create(cursor)
                self.copy(cursor)
            self.insert(cursor)
            if self.model is RiskscapeData:
                self.drop(cursor)

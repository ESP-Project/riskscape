from calendar import month_name

from django.utils.safestring import mark_safe
from django.conf import settings

MONTHS = {x: month_name[x] for x in range(1, 13)}

BMI_CHOICES = (
    (0, "Not measured"),
    (1, mark_safe("Normal (BMI&nbsp;&lt;25)")),
    (2, mark_safe("Overweight (BMI&nbsp;25-&lt;30)")),
    (3, mark_safe("Obese (BMI&nbsp;&gt;30.0)")),
)

BMI_PERCENT_CHOICES = (
    (0, "Not measured"),
    (1, mark_safe("&le;5th")),
    (2, "6-50th"),
    (3, "51-85th"),
    (4, "86-95th"),
    (5, mark_safe("&gt;95th")),
)

BP_CHOICES = (
    (0, "Not measured"),
    (1, mark_safe("Normal (&le;130)")),
    (2, mark_safe("Borderline (131-140)")),
    (3, mark_safe("High (&ge;141)")),
)

TG_CHOICES = (
    (0, "Not measured"),
    (1, "0-199"),
    (2, "200-499"),
    (3, mark_safe("&gt;500")),
)
LDL_CHOICES = (
    (0, "Not measured"),
    (1, "0-99"),
    (2, "100-129"),
    (3, "130-159"),
    (4, "160-189"),
    (5, mark_safe("&gt;189")),
)
A1C_CHOICES = (
    (0, "Not measured"),
    (1, "0-5.6"),
    (2, "5.7-6.4"),
    (3, "6.5-8.0"),
    (4, mark_safe("&gt;8.0")),
)

AGE_CHOICES = (
    (1, "0-4"),
    (2, "5-9"),
    (3, "10-14"),
    (4, "15-19"),
    (5, "20-24"),
    (6, "25-29"),
    (7, "30-39"),
    (8, "40-49"),
    (9, "50-59"),
    (10, "60-69"),
    (11, "70-79"),
    (12, mark_safe("&gt;80")),
)
SEX_CHOICES = (
    (1, "Male"),
    (2, "Female"),
)

RACE_ETHNICITY_CHOICES = settings.RACE_CATEGORIES

ETHNICITY_CHOICES = settings.ETHNICITY_CATEGORIES

SITE_CHOICES = settings.SITENAMES

SMOKING_CHOICES = (
    (0, "Not measured"),
    (1, "Never"),
    (2, "Passive"),
    (3, "Former"),
    (4, "Current"),
)
HYPERTENSION_CHOICES = settings.HTN_CATEGORIES
BOOL_CHOICES = (
    (1, "Yes"),
    (0, "No"),
)

BIRTH_COHORT_CHOICES = (
    (0, "Before 1945"),
    (1, "1945-1965"),
    (2, "After 1965"),
)

POS_NEG_IND_CHOICES = (
    (0, "No Test"),
    (1, "Indeterminate"),
    (2, "Negative"),
    (3, "Positive"),
)

SYPH_CHOICES = (
    (0, "No Test"),
    (1, "0-1 years ago"),
    (2, "1-2 years ago"),
    (3, mark_safe("&gt;2 years ago")),
)
CVD_CHOICES = (
    (1, "Known CV disease"),
    (2, mark_safe("&gt;20")),
    (3, "15-20"),
    (4, "10-15"),
    (5, "5-10"),
    (6, mark_safe("&lt;5")),
)
DIAGNOSED_HYPERTENSION_CHOICES = settings.DIAG_HTN_CATEGORIES
PRIMARY_PAYER_CHOICES = settings.PRIMARY_PAYERS
DIAGNOSED_DIABETES_CHOICES = (
    (4, "Not Diagnosed"),
    (0, "Well Controlled"),
    (1, "Controlled"),
    (2, "Uncontrolled"),
    (3, "Unmeasured"),
)
PLACE_CHOICES = settings.GEO_PLACE_CHOICES
